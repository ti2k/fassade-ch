function scrollTo(element) {
	jQuery('html, body').animate({
        scrollTop: element.offset().top
    }, 500, 'linear', function() {
    	jQuery('html, body').stop();
    });
}

function closeAccordion(link) {
	if (link.parent().hasClass('card-body')) {
		link.parent().parent().removeClass('show');
	}
	jQuery('.carousel-item .accordion .show').removeClass('show');
}

window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#000"
        },
        "button": {
          "background": "#005493"
        }
      },
      "theme": "classic",
      "content": {
        "message": cookieText,
        "dismiss": dismissText,
        "link": learnMoreText,
        "href": "?datenschutz=1#datenschutz"
      }
    });
});

$(document).ready(function() {
	jQuery('#carouselArchive > .carousel-inner > .carousel-item').each(function() {
		jQuery('<option/>', {'value': jQuery(this).attr('data-carouselIndex')}).html(jQuery(this).attr('data-jahr')).appendTo(jQuery('#jahrSelector'));
	});
	jQuery('#jahrSelector').on('change', function() {
		jQuery('#carouselArchive').carousel(parseInt(jQuery(this).val()));
	});
	
	jQuery('.linkToArchiv').on('click', function(e) {
		e.preventDefault();
		var clickedLink = jQuery(this);
		var ajaxData = {
			'setJahr': clickedLink.attr('data-jahr'),
			'setMonat': clickedLink.attr('data-monat'),
			'keyword': ''
		};
		var data = JSON.stringify(ajaxData);
		jQuery.ajax({
			url: 'include/ajax.inc.php',
			type: 'POST',
			data: {setAusgabe: 1, data: data},
		}).done(function(data) {
			data = jQuery.parseJSON(data);
			if (data.control == 'ok') {
				if (!data.loggedIn) {
					closeAccordion(clickedLink);
					jQuery.ajax({
						url: 'include/login.php',
						type: 'GET',
						data: {'ajaxCall': 1, 'select_jahr': clickedLink.attr('data-jahr'), 'select_monat': clickedLink.attr('data-monat'), 'keyword': ''},
					}).done(function(data) {
						jQuery('#content-middle-top').html(data);
						scrollTo(jQuery('#archiv'));
					});
				}
				else {
					jQuery('#keyword').val('');
					jQuery('#select_jahr > option').removeAttr('selected');
					jQuery('#select_jahr > option[value=' + clickedLink.attr('data-jahr') + ']').attr('selected', 'selected');
					jQuery('#select_jahr').val(clickedLink.attr('data-jahr'));
					jQuery('#select_monat > option').removeAttr('selected');
					jQuery('#select_monat > option[value=' + clickedLink.attr('data-monat') + ']').attr('selected', 'selected');
					jQuery('#select_monat').val(clickedLink.attr('data-monat'));
					jQuery('#content-middle-bottom').html('');
					jQuery.ajax({
						url: 'include/showPDF.php',
						type: 'GET',
						data: {'ajaxCall': 1},
					}).done(function(data) {
						jQuery('#content-middle-bottom').html(data);
					});
					closeAccordion(clickedLink);
					scrollTo(jQuery('#archiv'));
				}
			}
		});
	});
	
	jQuery('.btn-logout').on('click', function(e) {
		e.preventDefault();
		jQuery.ajax({
			url: 'include/ajax.inc.php',
			type: 'GET',
			data: {'logout': 1},
		}).done(function(data) {
			jQuery('#content-middle-top').html('');
			jQuery('#content-middle-bottom').html('');
			jQuery.ajax({
				url: 'include/login.php',
				type: 'GET',
				data: {'ajaxCall': 1, 'logout': 1},
			}).done(function(data) {
				jQuery('#content-middle-top').html(data);
				scrollTo(jQuery('#archiv'));
			});
		});
	});
	
	jQuery('#suchformular > form').on('submit', function(e) {
		e.preventDefault();
		var ajaxData = {
			'setJahr': jQuery('#select_jahr').val(),
			'setMonat': jQuery('#select_monat').val(),
			'keyword': jQuery('#keyword').val()
		};
		var data = JSON.stringify(ajaxData);
		jQuery.ajax({
			url: 'include/ajax.inc.php',
			type: 'POST',
			data: {setAusgabe: 1, data: data},
		}).done(function(data) {
			jQuery('#content-middle-bottom').html('');
			jQuery.ajax({
				url: 'include/showPDF.php',
				type: 'GET',
				data: {'ajaxCall': 1},
			}).done(function(data) {
				jQuery('#content-middle-bottom').html(data);
			});
		});
	});
	
	jQuery('#loginForm').on('submit', function(e) {
		e.preventDefault();
		jQuery.ajax({
			url: 'include/ajax.inc.php',
			type: 'POST',
			data: {'passwort': (jQuery('#passwort').val())},
			statusCode: {
			    400: function() {
			      jQuery('.alert.hidden').show();
			    }
			  }
		}).done(function(data) {
			jQuery('#content-middle-top').html('');
			jQuery('#content-middle-bottom').html('');
			jQuery.ajax({
				url: 'include/suchformular.php',
				type: 'GET',
				data: {'ajaxCall': 1},
			}).done(function(data) {
				jQuery('#content-middle-top').html(data);
				jQuery.ajax({
					url: 'include/showPDF.php',
					type: 'GET',
					data: {'ajaxCall': 1},
				}).done(function(data) {
					jQuery('#content-middle-bottom').html(data);
					scrollTo(jQuery('#archiv'));
				});
			});
		});
	});
	
	jQuery('#fassaden_archiv').on('submit', function(e) {
		e.preventDefault();
		jQuery.ajax({
			url: 'include/statistik_inhalt.php',
			type: 'POST',
			data: {'btn_setFilterStat': 1, 'statFrom': jQuery('#statFrom').val(), 'statTo': jQuery('#statTo').val(), 'ajaxCall': 1},
		}).done(function(data) {
			jQuery('.statErgebnis').html(data);
		});
	});
	
	if (jQuery('#statistik')) {
		var $j = jQuery.noConflict();
		$j("#statFrom").datepicker();
		
		$j(function(){
			$j("#statFrom").datepicker({
				defaultDate: new Date($j('#statFrom').attr('data-date')),
				onClose: function( selectedDate ) {
	                $j( "#statTo" ).datepicker( "option", "minDate", selectedDate );
	            }
			});
			$j("#statFrom").datepicker("option", $j.datepicker.regional['de-CH']);		
	
			$j("#statTo").datepicker({
				defaultDate: new Date($j('#statTo').attr('data-date'))
			});
			$j("#statTo").datepicker("option", $j.datepicker.regional['de-CH']);
		});
	}
	
	jQuery('#fassadeModal').on('show.bs.modal', function (event) {
	  var button = jQuery(event.relatedTarget); 
	  var teaser = button.next('div.teaserText').html();
	  var title = button.html();
	  var idx = button.data('index');
	  var modal = jQuery(this);
	  modal.find('.modal-title').text(title);
	  modal.find('.modalText').html(teaser);
	  modal.find('.modalImage').html('');
	  jQuery('#aktuellCover').clone().appendTo(modal.find('.modalImage'));
	  modal.find('a').attr('href', '?select_jahr=' + jQuery('.biblioButton').data('jahr') + '&select_monat=' + jQuery('.biblioButton').data('monat') + '&keyword=' + encodeURIComponent(title.substring(0, 12)) + '#archiv')
	});
});