<?php
session_start();
require_once("config.php");
if (isset($_SESSION['login']) && ($_SESSION['login'] == 1) && !isset($_SESSION['admin'])) {
    unset($_SESSION['importLogin']);
    header('Location: /');
}

function transString($string) {
    $trans = array(
        '...'                => '&#8230;',
        '\\205'                => '&#8230;',
        '\\221'                => chr(145),
        '\\222'                => chr(146),
        '\\223'                => chr(147),
        '\\224'                => chr(148),
        '\\226'                => '-',
        '\\267'                => '&#8226;',
        '\\('                => '(',
        '\\['                => '[',
        '##ENDBRACKET##'    => ')',
        '##ENDSBRACKET##'    => ']',
        chr(133)            => '-',
        chr(141)            => chr(147),
        chr(142)            => chr(148),
        chr(143)            => chr(145),
        chr(144)            => chr(146),
    );
    $string = strtr($string, $trans);
    return $string;
}
?>
<html>
<head>
  <title>Fassade - SZFF</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  <link type="text/css" rel="stylesheet" href="stylesheet/jquery-ui.css" />
  <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
  <link type="text/css" rel="stylesheet" href="css/style.css" />
  <base href="<?php echo _BASE_URL; ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
</head>
<body>
<section>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<h1>Import Archiv</h1>
				<?php 
				if (isset($_SESSION['admin'])) {
				
				if (isset($_POST['import']) && ($_FILES['csv']['name'] != '') && ($_FILES['cover']['name'] != '')) {
				    //echo '<pre>'.readfile($_FILES['csv']['tmp_name']).'</pre>';
				    $importData = array();
				    $datei = fopen($_FILES['csv']['tmp_name'], "r");
				    while ($daten = fgetcsv($datei, 100000, ';', '"')) {
				        if (($daten[0] == '') || ($daten[1] == 'Rubrik')) continue;
				        $temp = array();
				        $temp['fld_jahr'] = $_POST['select_jahr'];
				        $temp['fld_monat'] = $_POST['select_monat'];
				        $temp['fld_txt'] = $_POST['select_monat'] . '-' . $_POST['select_jahr'] . ' FASSADE | FAÇADE - ';
				        $temp['idx_archiv'] = '';
				        $temp['fld_pdf'] = $daten[0];
				        $temp['fld_artikelname'] = ($daten[1]);
				        $temp['fld_txt'].= ($daten[1]);
				        $temp['fld_titel'] = ($daten[2]);
				        $temp['fld_txt'].= ($daten[2]);
				        $temp['fld_teaser'] = $daten[3]; //mb_substr(($daten[3]), 0, 1000);
				        $temp['fld_txt'].= ($daten[3]).'

';
				        $temp['fld_artikelname_fr'] = ($daten[4]);
				        $temp['fld_titel_fr'] = ($daten[5]);
				        $temp['fld_txt'].= ($daten[5]);
				        $temp['fld_teaser_fr'] = $daten[6]; //mb_substr(($daten[6]), 0, 1000);
				        $temp['fld_txt'].= ($daten[6]);
				        
				        $importData[] = $temp;				        
				    }
				    //echo '<pre>'.print_r($importData, true).'</pre>';
				    $dbConn = mysqli_connect(_HOST, _USER, _PW);
				    mysqli_select_db($dbConn, _DB);
				    mysqli_query($dbConn, "DELETE FROM tbl_archiv WHERE fld_jahr=".$_POST['select_jahr']." AND fld_monat=".$_POST['select_monat']);
				    foreach($importData as $data) {
				        $sql->fDoInsert('tbl_archiv', $data);
				    }
				    
				    $coverFilename = 'media/cover/' . $_POST['select_jahr'] . '_' . sprintf('%02s', $_POST['select_monat']) . substr($_FILES['csv']['name'], stripos($_FILES['csv']['name'], '.'));
				    if (file_exists($coverFilename)) {
				        unlink($coverFilename);
				    }				    
				    move_uploaded_file($_FILES['cover']['tmp_name'], $coverFilename);
				    echo '<div class="alert-success">Import erfolgreich!</div>';
        		}
        		else if (isset($_POST['import'])) {
        		    echo '<div class="alert-danger">Bitte laden Sie beide Dateien zusammen hoch!</div>';
        		}
        		?>
				
        		<form id="importArchiv" method="post" enctype="multipart/form-data">
        		<div class="form-group">
        			<label for="select_jahr"><?php echo __('year'); ?></label>
        			<select id="select_jahr" name='select_jahr' class='form-control'> 
        				<?php
        				sort($queryDefineYear);
        				for($a=date("Y"); $a <= ((int)(date("Y"))+4); $a++){
        				    echo '<option value= "'.$a.'"'.((isset($_POST['select_jahr']) && ($_POST['select_jahr'] == $i))? ' selected':'').'> '.$a.'</option>';
        				}
        				?>
        			</select>
        		</div>
        		<div class="form-group">
        			<label for="select_monat"><?php echo __('month'); ?></label>
        			<select id="select_monat" name='select_monat' class='form-control'> 
        				<?php
        				for($i=1; $i <= 4; $i++){
       					  echo '<option value= "'.$i.'"'.((isset($_POST['select_monat']) && ($_POST['select_monat'] == $i))? ' selected':'').'> '.$i.'</option>';
        				}
        				?>
        			</select>
        		</div>
        		<div class="form-group">
        			<label for="csv">CSV-Datei utf-8</label>
        			<input type="file" name="csv" id="csv" class="form-control">
        		</div>
        		<div class="form-group">
        			<label for="cover">Cover jpg</label>
        			<input type="file" name="cover" id="cover" class="form-control">
        		</div>
        		<button type="submit" name="import" class="btn btn-submit">Importieren</button>
        		</form>
        		<?php 
				}
				else {
				    $_SESSION['importLogin'] = 1;
				    include('include/login.php');
				}
        		?>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4"><strong>Herausgeber und Verlag:</strong><br />Schweizerische Zentrale Fenster und Fassaden SZFF<br />Ringstrasse 15, CH-4600 Olten<br />www.szff.ch</div>
			<div class="col-md-4">®Fassade | Façade 2018</div>
			<div class="col-md-4">
				<nav class="footerNavigation">
					<ul class="list-inline">
						<?php /* 
						<li class="list-inline-item"><a href="">Kontakt</a></li>
						<li class="list-inline-item"><a href="">Sitemap</a></li>
						*/ ?>
						<li class="list-inline-item"><a href="?impressum=1">Impressum</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>
<script src="js/jQuery.3.3.1.min.js"></script>
<script src="js/jQueryUI.1.9.1.js"></script>
<script src="js/jQuery-Datepicker-CH.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scriptImport.js"></script>
</body>
</html>