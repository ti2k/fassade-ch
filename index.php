<?php
session_start();

//Bei den zwei untenliegenden Zeilen handelt es sich um Code, der die Fehlermeldungen veranschaulichen lässt
ini_set('display_errors', false);

require_once("config.php");

//Die Sprache wird in eine Session geschrieben
if((isset($_GET['language']) && $_GET['language'] == 0) || (isset($_GET['language']) && $_GET['language'] == 1)){
	$_SESSION['language'] = $_GET['language'];
}elseif(!isset($_SESSION['language'])){
	$_SESSION['language'] = 0;
}

//Überprüft ob die Get-Variable "logout" gesetzt ist und ob sie den Wert 1 hat, wenn dies der Fall ist soll die Session gelöscht werden
//Wenn diese Abfrage stimmt wird der Benutzer ausgeloggt
if(isset($_GET['logout']) && $_GET['logout'] == 1){
	$_SESSION['login'] = 0;
	unset($_SESSION['admin']);
	session_destroy();
}

//Bei klick auf das SZFF Logo wird man auf die Startseite verlinkt, alle Suchkriterien werden gelöscht
if(isset($_GET['start']) && $_GET['start'] == 1){
	unset($_SESSION['searchKeyword']);
	unset($_SESSION['searchYear']);
	unset($_SESSION['searchMonth']);
}

//Adminpasswort auslesen und überprüfen
if(isset($_POST['passwort'])){
	$queryAdminLogin = "SELECT * FROM `tbl_passwort` WHERE fld_passwort = '" .  md5($_POST['passwort']) . "' AND fld_isAdmin = 1";
	$sqlAdminLogin = $sql->fDoQuery($queryAdminLogin);
}

//Überprüft ob ein Eintrag bei der SQL abfrage ausgelesen wurde
if(isset($sqlAdminLogin) && $sqlAdminLogin != "" && count($sqlAdminLogin) > 0){
	$_SESSION['login'] = 1;
	$_SESSION['admin'] = 1;
	if (isset($_SESSION['importLogin'])) {
	    unset($_SESSION['importLogin']);
	    header('Location: FO_import.php');
	}
}

// Monatslogin auslesen und überprüfen
$timestamp = time();
if(isset($_POST['passwort'])){
	$queryLogin = "SELECT * FROM `tbl_passwort` WHERE fld_passwort = '" .  md5($_POST['passwort']) . "' AND fld_isAdmin = 0 AND '" . $timestamp . "' >= fld_start AND '" . $timestamp . "' <= fld_end";
	$sqlLogin = $sql->fDoQuery($queryLogin);
}

//Überprüft ob ein Eintrag bei der SQL abfrage ausgelesen wurde
if(isset($sqlLogin) && $sqlLogin != "" && count($sqlLogin) > 0){
	$_SESSION['login'] = 1;
}

if (isset($_SESSION['importLogin'])) {
    header('Location: FO_import.php');
}

?>
<html>
<head>
    <title>Fassade/Facade - SZFF</title>
		<meta name="title" content="Fassade/Facade - SZFF">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="15 days">
		<meta property="og:title" content="Fassade/Facade - SZFF" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"]; ?>" />
		<?php
		$select_query = "SELECT fld_monat, fld_jahr FROM `tbl_archiv` WHERE 1 GROUP BY fld_jahr, fld_monat ORDER BY `fld_jahr` DESC, `fld_monat` DESC LIMIT 1";
		$result = $sql->fDoQuery($select_query);


    if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) {
		?>
		<meta name="description" content="Revue technique pour fenêtres et façades">
		<meta name="keywords" content="Facade, Revue">
		<meta property="og:description" content="Revue technique pour fenêtres et façades" />
		<?php
		} else {
		?>
		<meta name="description" content="Fachzeitschrift für Fenster- und Fassadenbau">
		<meta name="keywords" content="Fassade, Fachzeitschrift">
		<meta property="og:description" content="Fachzeitschrift für Fenster- und Fassadenbau" />
		<?php
	  } ?>
		<meta name="author" content="SZFF/CSFF | Schweizerische Zentrale Fenster und Fassaden">

		<?php
		$image_file = 'media/cover/2018_04.jpg';
		if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.jpg')) {
				$image_file = 'https://'.$_SERVER["HTTP_HOST"].'/media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.jpg';
		}
		else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.png')) {
				$image_file = 'https://'.$_SERVER["HTTP_HOST"].'/media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.png';
		}
		else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.jpg')) {
				$image_file = 'https://'.$_SERVER["HTTP_HOST"].'/media/cover/' . $result['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.jpg';
		}
		else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.png')) {
				$image_file = 'https://'.$_SERVER["HTTP_HOST"].'/media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.png';
		}
		?>
		<meta property="og:image" content="<?php echo $image_file; ?>" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="stylesheet/jquery-ui.css" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <base href="<?php echo _BASE_URL; ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
    <link rel="apple-touch-icon" sizes="120x120" href="/media/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/media/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/media/favicon-16x16.png">
    <link rel="manifest" href="/media/site.webmanifest">
    <link rel="mask-icon" href="/media/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/media/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/media/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <script>
    <?php
    if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) {
        echo '
            var cookieText = "Les cookies augmentent la convivialité de ce site. En utilisant notre site Web, vous acceptez que nous utilisions des cookies.";
            var dismissText = "OK";
            var learnMoreText = "Plus d\'informations";';
    }
    else {
        echo '
            var cookieText = "Cookies erhöhen die Benutzerfreundlichkeit dieser Website. Mit der Nutzung unserer Website erklären Sie sich damit einverstanden, dass wir Cookies verwenden.";
            var dismissText = "OK";
            var learnMoreText = "Mehr Informationen";';
    }
    ?>
    </script>
</head>
<body>
    <header>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-xs-12 w-100"><?php
        		$headImages = array();
        		$dir_handle = opendir('media/headimages/');
        		while (false !== ($entry = readdir($dir_handle))) {
        		    if (($entry == '.') || ($entry == '..')) continue;
        		    $headImages[] =  $entry;
        		}
        		echo '<img src="media/headimages/' . $headImages[random_int(0, count($headImages)-1)]. '" alt="Titelbild" class="img-fluid w-100">';
        		?></div>
        	</div>
        </div>
        <div class="langnavi">
        	<div class="container">
        		<nav class="langNavigation">
                	<ul>
                		<li class="btn btn_lang<?php if (!isset($_SESSION['language'] ) || ($_SESSION['language']  == 0)) echo ' aktivLang'; ?>"><a href="?language=0" id="de">Deutsch</a></li>
        				<li class="btn btn_lang<?php if (isset($_SESSION['language'] ) && ($_SESSION['language']  == 1)) echo ' aktivLang'; ?>"><a href="?language=1" id="fr">Fran&ccedil;ais</a></li>
                	</ul>
            	</nav>
            </div>
        </div>
    	<div class="logoContainer">
    		<div class="container">
        		<div class="row justify-content-center align-items-center">
        			<div class="col-md-6 text-center">
        				<a href="/"><img src="media/Fassade_Logo.png" alt="Logo" class="img-fluid" title="FASSADE - FAÇADE"></a><br /><strong>Fachzeitschrift für Fenster- und Fassadenbau</strong><br />Revue technique pour fenêtres et façades
        			</div>
        		</div>
        	</div>
    	</div>
    	<div class="nextButton"><a href="#aktuell"><img src="media/pfeil_unten.png" alt="zum Inhalt" titel="zum Inhalt"></a></div>
    </header>

    <?php
    if (isset($_REQUEST['impressum'])) {
    #
    #   Zeige das Impressum
    #
    ?>
    <section>
    	<div class="container">
    		<div class="row">
    <?php
        include('include/impressum.php');
    ?>
        	</div>
        </div>
    </section>
    <?php
    }
    else if (isset($_REQUEST['datenschutz'])) {
        #
        #   Zeige das Impressum
        #
        ?>
    <section>
    	<div class="container">
    		<div class="row">
    <?php
        include('include/datenschutz.php');
    ?>
        	</div>
        </div>
    </section>
    <?php
    }
    else if (isset($_REQUEST['abo'])) {
        #
        #   Zeige das Impressum
        #
        ?>
    <section>
    	<div class="container">
    		<div class="row">
    <?php
        include('include/abonnement.php');
    ?>
        	</div>
        </div>
    </section>
	<?php
    }
    else if (isset($_REQUEST['leitfaden'])) {
        #
        #   Zeige den Leitfaden
        #
        ?>
    <section>
    	<div class="container">
    		<div class="row">
    <?php
        include('include/leitfaden.php');
    ?>
        	</div>
        </div>
    </section>
    <?php
    }
    else {
    #
    #   Zeige den One-Pager
    #
    ?>
    <section class="aktuelleAusgabe" id="aktuell">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-7">
    				<div class="ausgabenKopf">FASSADE | FAÇADE <?php echo $result[0]['fld_monat'] . '/' . $result[0]['fld_jahr']; ?></div>
					<h2><?php echo __('aktuelleAusgabe'); ?></h2>
					<p><?php echo __('aktuelleAusgabeSubline'); ?></p>
        			<div class="accordion" id="accordionAktuell">

        			<?php
        			$inner_select = "SELECT * FROM `tbl_archiv` WHERE fld_titel!='' AND fld_jahr=".$result[0]['fld_jahr']." AND fld_monat=".$result[0]['fld_monat']." AND (`fld_artikelname` LIKE '%TECHNIK%' OR `fld_artikelname` LIKE '%REPORT%' OR `fld_artikelname` LIKE '%NETWORK%') ORDER BY `fld_artikelname` DESC";
                  	$inner_result = $sql->fDoQuery($inner_select);
                  	$artikelname = '';
                  	$sectionKey=0;
                  	foreach($inner_result as $artkey => $artikel) {
                  	    // When title is empty jump over
                  	    if (trim($artikel['fld_titel']) == '') {
                  	        continue;
                  	    }
                  	    if (stripos($artikel['fld_artikelname'], 'Technik') !== false) {
                  	        $nameGefunden = __('technik');
                  	    }
                  	    else if (stripos($artikel['fld_artikelname'], 'Report') !== false) {
                  	        $nameGefunden = __('report');
                  	    }
                  	    else {
                  	        $nameGefunden = ($artikel['fld_artikelname']);
                  	    }

                  	    if ($artikelname != $nameGefunden) {
                  	        $artikelname = $nameGefunden;
                  	        $sectionKey++;
                  	        if ($artkey > 0) {
                  	            echo '</ul>
                            </div>
                        </div>
                      </div>';
                  	        }
                  	        echo '
                      <div class="card">
                        <div class="card-header" id="heading'.$sectionKey.'">
                          <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$sectionKey.'" aria-expanded="false" aria-controls="collapse'.$sectionKey.'">
                              '.($artikelname).'
                            </button>
                          </h5>
                        </div>
';
                  	        echo '
                        <div id="collapse'.$sectionKey.'" class="collapse" aria-labelledby="heading'.$sectionKey.'" data-parent="#accordionAktuell">
                          <div class="card-body"><ul>
';
                  	    }
                  	    $trans = array( '...' => '…', '\\205' => '…', '\\221' => chr(145), '\\222' => chr(146), '\\223' => chr(147), '\\224' => chr(148), '\\226' => '-', '\\267' => '•', '\\(' => '(', '\\[' => '[', '##ENDBRACKET##' => ')', '##ENDSBRACKET##' => ']', chr(133) => '-', chr(141) => chr(147), chr(142) => chr(148), chr(143) => chr(145), chr(144) => chr(146), );
                  	    echo '<li>';
                  	    if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) {
                  	        echo '<a data-toggle="modal" data-target="#fassadeModal" data-index="'.$artikel['idx_archiv'].'">' . ($artikel['fld_titel_fr']) . '</a>';
                  	        echo '<div class="teaserText hidden">' . $artikel['fld_teaser_fr'] . '</div>';
                  	    }
                  	    else {
                  	        echo '<a data-toggle="modal" data-target="#fassadeModal" data-index="'.$artikel['idx_archiv'].'">' . ($artikel['fld_titel']) . '</a>';
                  	        echo '<div class="teaserText hidden">' . $artikel['fld_teaser'] . '</div>';
                  	    }
                  	    echo '</li>';
                  	}
                  	?>		</ul>
                          </div>
                        </div>
                      </div>
                    </div>
    			</div>
    			<?php
		        $image_file = 'media/cover/2018_04.jpg';
		        if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.jpg')) {
		            $image_file = 'media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.jpg';
            }
            else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.png')) {
                $image_file = 'media/cover/' . $result[0]['fld_jahr'] . '_' . sprintf('%02s', $result[0]['fld_monat']) . '.png';
            }
            else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.jpg')) {
                $image_file = 'media/cover/' . $result['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.jpg';
            }
            else if (file_exists('media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.png')) {
                $image_file = 'media/cover/' . $result[0]['fld_jahr'] . '_' . $result[0]['fld_monat'] . '.png';
            }
          ?>
    			<div class="col-md-5 order-first text-center"><div class="coverBox text-center"><div class="coverSchatten"></div><img src="<?php echo $image_file; ?>" alt="Cover Ausgabe <?php echo $result[0]['fld_monat'] . '/' . $result[0]['fld_jahr']; ?>" class="img-fluid" id="aktuellCover"></div></div>
        	</div>
        </div>
        <div class="container">
    		<div class="row justify-content-center aktuellButton">
    			<div class="col-md-3 text-center"><a href="?abo=1#abo" class="btn btn-default w-100" role="button">Abonnement</a></div>
               	<div class="col-md-3 text-center"><a href="mailto:fassade@szff.ch?subject=<?php echo __('probenummer_subject'); ?>" class="btn btn-default w-100" role="button"><?php echo __('probenummer'); ?></a></div>
               	<div class="col-md-3 text-center"><a href="?select_jahr=<?php echo $result[0]['fld_jahr']; ?>&select_monat=<?php echo $result[0]['fld_monat']; ?>&keyword=#archiv" class="btn biblioButton linkToArchiv w-100" data-jahr="<?php echo $result[0]['fld_jahr']; ?>" data-monat="<?php echo $result[0]['fld_monat']; ?>"><?php echo __('artikelbibliothek'); ?></a></div>
            </div>
    	</div>
    </section>
    <section class="ausgaben" id="ausgaben">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-12">
    				<h2 class="col-xs-12 text-center"><?php echo __('fruehereAusgaben');?></h2>
    				<div class="col-xs-12 text-center jahresauswahl"><select id="jahrSelector" class="form-control"></select></div>
        			<div id="carouselArchive" class="carousel slide" data-ride="carousel" data-interval="false">
                      <div class="carousel-inner">
                      <?php
                      $select_query = "SELECT fld_monat, fld_jahr FROM `tbl_archiv` WHERE fld_jahr>".date('Y', mktime(0,0,1,1,1,date('Y')-4))." AND (`fld_artikelname` LIKE '%TECHNIK%' OR `fld_artikelname` LIKE '%REPORT%' OR `fld_artikelname` LIKE '%NETWORK%') GROUP BY fld_jahr, fld_monat ORDER BY `fld_jahr` DESC, `fld_monat` DESC";
                      $result = $sql->fDoQuery($select_query);
                      $jahr = 0;
                      $ausgabe = 0;
                      $carouselIndex = 0;
                      //echo '<pre>'.print_r($result,true).'</pre>';

                      foreach ($result as $key => $ausgabe) {
                          if ($jahr != $ausgabe['fld_jahr']) {
                              $jahr = $ausgabe['fld_jahr'];
                              if ($key > 0) {
                      ?>
                             </div>
                        </div>
                      <?php
                              }
                      ?>
                        <div class="carousel-item <?php if ($key == 0) echo 'active'; ?>" data-carouselIndex="<?php echo $carouselIndex++; ?>" data-jahr="<?php echo $jahr; ?>">
                        	<div class="row">
                      <?php
                          } // if ($jahr != $ausgabe['fld_jahr'])
                          $image_file = 'media/cover/2017_1.jpg';
                          if (file_exists('media/cover/' . $ausgabe['fld_jahr'] . '_' . sprintf('%02s', $ausgabe['fld_monat']) . '.jpg')) {
                              $image_file = 'media/cover/' . $ausgabe['fld_jahr'] . '_' . sprintf('%02s', $ausgabe['fld_monat']) . '.jpg';
                          }
                          else if (file_exists('media/cover/' . $ausgabe['fld_jahr'] . '_' . sprintf('%02s', $ausgabe['fld_monat']) . '.png')) {
                              $image_file = 'media/cover/' . $ausgabe['fld_jahr'] . '_' . sprintf('%02s', $ausgabe['fld_monat']) . '.png';
                          }
                          else if (file_exists('media/cover/' . $ausgabe['fld_jahr'] . '_' . $ausgabe['fld_monat'] . '.jpg')) {
                              $image_file = 'media/cover/' . $ausgabe['fld_jahr'] . '_' . $ausgabe['fld_monat'] . '.jpg';
                          }
                          else if (file_exists('media/cover/' . $ausgabe['fld_jahr'] . '_' . $ausgabe['fld_monat'] . '.png')) {
                              $image_file = 'media/cover/' . $ausgabe['fld_jahr'] . '_' . $ausgabe['fld_monat'] . '.png';
                          }
                      ?>
                            	<div class="col-md-3">
                            		<div class="coverPicture"><img src="<?php echo $image_file; ?>" alt="<?php echo $ausgabe['fld_monat'] . '/' . $ausgabe['fld_jahr']; ?>" class="img-fluid"></div>
                            		<div class="accordion" id="accordion<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>">
                                      <div class="card">
                                        <div class="card-header" id="heading<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>">
                                          <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>" aria-expanded="false" aria-controls="collapse<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>">
                                              <?php echo $ausgabe['fld_monat'] . '/' . $ausgabe['fld_jahr']; ?>
                                            </button>
                                          </h5>
                                        </div>
                                        <div id="collapse<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>" class="collapse" aria-labelledby="heading<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>" data-parent="#accordion<?php echo $ausgabe['fld_monat'] . $ausgabe['fld_jahr']; ?>">
                                          <div class="card-body">
                                          	<?php
                                          	$inner_select = "SELECT * FROM `tbl_archiv` WHERE fld_jahr=".$ausgabe['fld_jahr']." AND fld_monat=".$ausgabe['fld_monat']." AND (`fld_artikelname` LIKE '%TECHNIK%' OR `fld_artikelname` LIKE '%REPORT%' OR `fld_artikelname` LIKE '%NETWORK%') ORDER BY `fld_artikelname` DESC";
                                          	$inner_result = $sql->fDoQuery($inner_select);
                                          	$artikelname = '';
                                          	foreach($inner_result as $artkey => $artikel) {
                                          	    if (trim($artikel['fld_titel']) == '') {
                                          	        continue;
                                          	    }
                                          	    if (stripos($artikel['fld_artikelname'], 'Technik') !== false) {
                                          	        $nameGefunden = __('technik');
                                          	    }
                                          	    else if (stripos($artikel['fld_artikelname'], 'Report') !== false) {
                                          	        $nameGefunden = __('report');
                                          	    }
                                          	    else {
                                          	        $nameGefunden = ($artikel['fld_artikelname']);
                                          	    }

                                          	    if ($artikelname != $nameGefunden) {
                                          	        $artikelname = $nameGefunden;
                                          	        if ($artkey > 0) {
                                          	            echo '</ul>';
                                          	        }
                                          	        echo '<p><strong>'.$artikelname.'</strong></p>';
                                          	        echo '<ul>';
                                          	    }

                                          	    echo '<li>';
                                          	    if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) {
                                          	        echo ($artikel['fld_titel_fr']);
                                          	    }
                                          	    else {
                                          	        echo ($artikel['fld_titel']);
                                          	    }
                                          	    echo '</li>';
                                          	}
                                          	?>
                                          	</ul>
                                          	<a href="?select_jahr=<?php echo $ausgabe['fld_jahr']; ?>&select_monat=<?php echo $ausgabe['fld_monat']; ?>&keyword=#archiv" class="btn biblioButton linkToArchiv" data-jahr="<?php echo $ausgabe['fld_jahr']; ?>" data-monat="<?php echo $ausgabe['fld_monat']; ?>"><?php echo __('artikelbibliothek'); ?></a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                            	</div>
                        <?php
                        } // ENDE eines Jahres im Archive
                        ?>
                            </div>
                        </div>
                      </div>
                      <a class="carousel-control-prev" href="#carouselArchive" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselArchive" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
    			</div>
    		</div>
    	</div>
    </section>
    <section class="archive" id="archiv">
    	<div class="container">
    		<div class="row justify-content-center">
    			<div class="col-md-9">
        			<h2 class="col-xs-12 text-center"><?php echo __('artikelbibliothek');?></h2>
            		<?php
                    include('include/showArchive.php');
                     ?>
             	</div>
    		</div>
    	</div>
    </section>
    <section class="mediadaten" id="mediadaten">
    	<div class="container">
    		<div class="row justify-content-center">
    			<div class="col-md-9 text-center">
    				<h2 class="col-xs-12 text-center"><?php echo __('mediadaten');?> 2021</h2>
    				<a class="pdf-icon" href="media/mediadaten/MediadatenFassade2021<?php if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) echo '_f'; else echo '_d'; ?>.pdf" target="_blank"><i class="far fa-file-pdf" aria-hidden="true"></i></a><br />
    				<p><?php echo __('download_pdf');?></p>
    			</div>
    		</div>
    	</div>
    </section>
    <section class="bottomButtons">
    	<div class="container">
    		<div class="row justify-content-center">
    			<div class="col-md-3 text-center"><a href="http://www.szff.ch/index.asp?inc=<?php if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) echo 'fr/csff_szff/publications_shop.asp'; else echo 'szff_csff/publikationen_shop.asp'; ?>" target="_blank" class="btn btn-default" role="button"><?php echo __('onlineshop');?></a></div>
    			<div class="col-md-3 text-center"><a href="http://www.szff.ch/" target="_blank" class="btn btn-default" role="button"><?php echo __('szff');?></a></div>
				<div class="col-md-3 text-center"><a href="?leitfaden=1#leitfaden" class="btn btn-default" role="button"><?php echo __('leitfaden');?></a></div>
				
    		</div>
    	</div>
    </section>
    <?php
    }
    ?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4"><?php echo __('footer');?></div>
				<div class="col-md-4">®Fassade | Façade 2019</div>
				<div class="col-md-4">
					<nav class="footerNavigation">
						<ul class="list-inline">
							<?php /*
							<li class="list-inline-item"><a href="">Kontakt</a></li>
							<li class="list-inline-item"><a href="">Sitemap</a></li>
							*/ ?>
							<li class="list-item"><a href="?impressum=1#impressum"><?php echo __('impressum');?></a></li>
							<li class="list-item"><a href="?datenschutz=1#datenschutz"><?php echo __('datenschutz');?></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal fade" id="fassadeModal" tabindex="-1" role="dialog" aria-labelledby="fassadeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="fassadeModalLabel">New message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
			<div class="row">
				<div class="col-md-4 modalImage"></div>
				<div class="col-md-8 modalText"></div>
			</div>
          </div>
          <div class="modal-footer">
            <a href="" class="btn btn-primary">Artikel in Artikelbibliothek ansehen</a>
          </div>
        </div>
      </div>
    </div>
    <script src="js/jQuery.3.3.1.min.js"></script>
    <script src="js/jQueryUI.1.9.1.js"></script>
    <script src="js/jQuery-Datepicker-CH.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>
