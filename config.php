<?php
	// DB Conn
	define('_HOST', 'localhost');
	define('_USER', 'archiv_szff_db');
	define('_PW', 'bnS3w0!0');
	define('_DB', 'archiv_szff_neu');

	// pfade
	define('_DIR_ROOT', $_SERVER['DOCUMENT_ROOT']);

	// limits, voreinstellungen, etc.
	//define('_BASE_URL', 'https://fassade.ch.cyberfactory.swiss');
	if ($_SERVER["HTTP_HOST"] == 'archiv.szff.ch')
		define('_BASE_URL', 'https://archiv.szff.ch');
	else
		define('_BASE_URL', 'https://www.fassade.ch');
	define('_URL_PDF', '/fileadmin/pdf/');

	//language.php einbinden
	require_once("include/language.php");

	//Die Klasse class.db.php einbinden
	require_once("include/class.db.php");

	//Ein neues Objekt von der Klasse cDB erstellen mit dem Werten (_DB, _HOST, _USER, _PW)
	$sql = new cDB(_DB, _HOST, _USER, _PW);
?>
