<?php
	// Klassendefinition
	
	class cDB {
		### Variablen Definitionen ###
		/**
		 * Datenbankname
		 * @var string
		 */
		private $vDb = '';
		/**
		 * DIE Meldung, wenn ein Fehler auftritt
		 * @var string
		 */
		private $vDieM = 'Keine Verbindung zur Datenbank.';
		/**
		 * DB Host
		 * @var string
		 */
		private $vHost = '';
		/**
		 * DB Benutzername
		 * @var string
		 */
		private $vUser = '';
		/**
		 * DB Passwort
		 * @var string
		 */
		private $vPass = '';
		/**
		 * SQL String
		 * @var string
		 */
		private $vSql = '';
		/**
		 * Status, ob zur DB verbunden ist oder nicht
		 * @var bool
		 */
		private $vConnected = 0;
		/**
		 * Die MySQL Ressource ID bei einer Verbindung, sonst false
		 * @var bool/MySQL Ressource ID
		 */
		private $vConnection = 0;
		/**
		 * Wenn eine Abfrage gemacht wurde ist hier das Resultat gespeichert. Sonst ist es false.
		 * @var bool/array
		 */
		private $vResult = 0;
		
		/**
		 * Der Ordner, in dem die MySQL Commands (mysqldump, ...) sind
		 * @var string
		 */
		private $vMySQLDir = '/usr/bin';
		
		/**
		 * Der Ordner, an dem das Backup geschrieben wird
		 * @var string
		 */
		private $vBackupDir = '';
		
		/**
		 * Ordner fuer das Temporaere Backup vor dem Zippen
		 * @var string
		 */
		private $vTempBackupDir = '';
		
		/**
		 * Namen der Tabellen, die nicht ins Backup sollen
		 * @var string
		 */
		private $vExcludeDb = 'information_schema,test';
		
		/**
		 * Scriptlaufzeit beim DB Dump
		 * @var integer
		 */
		private $vExecutionTime = 18000;
		
		/**
		 * Ordner, in dem sich WinRar befindet
		 * @var string
		 */
		private $vRarDir = '';
		### ---------------------- ###
		
		/**
		 * Konstruktor
		 * @return
		 * @param string $pDb[optional]
		 * @param string $pHost[optional]
		 * @param string $pUser[optional]
		 * @param string $pPass[optional]
		 */
		function __construct($pDb = "", $pHost = "", $pUser = "", $pPass = ""){
			$this->vDb = $pDb;
			$this->vHost = $pHost;
			$this->vUser = $pUser;
			$this->vPass = $pPass;
		}
		
		/**
		 * Funktion, um zur DB zu Connecten
		 * @return bool
		 */
		function fConnect(){
			$this->vConnection = mysqli_connect($this->vHost, $this->vUser, $this->vPass);
			if (!$this->vConnection){
				$this->vConnected = 0;
				print_r($this->vDieM);
				return false;
			};
		
			if(mysqli_select_db($this->vConnection, $this->vDb)){
				$this->vConnected = 1;
				//$this->fDoQuery('SET NAMES utf8');
				return true;
			}else{
				$this->vConnected = 0;
				print_r($this->vDieM);
				return false;
			};
		}
		
		/**
		 * Funktion, um die Verbindung zur DB zu trennen
		 * @return bool
		 */
		function fDisConnect(){
			if($this->vConnected == 1){
				if(mysqli_close($this->vConnection)){
					$this->vConnected = 0;
					$this->vConnection = 0;
					return true;
				}else{
					return false;
				};
			}else{
				$this->vConnected = 0;
				$this->vConnection = 0;
				return true;
			};
		}
		
		/**
		 * Funktion, um Werte in die Datenbank zu schreiben
		 * @return int(mysql_insert_id)
		 * @param string $pTable[optional]
		 * @param array $pValues[optional]
		 */
		function fDoInsert($pTable = "", $pValues = array()){
			// verbindung herstellen
			$this->fConnect();

			if(($this->vConnection==false) or ($pTable=="") or (count($pValues)==0)){
				print_r($this->vDieM);
				return false;
			};

			$fFields = array_keys($pValues);
			
			$sInsert = "";
			$sInsert .= "INSERT INTO `" . $pTable . "` ( ";
			
			for($i=0;$i<=count($pValues)-1;$i++){
				if($i>0){
					$sInsert .= ", ";
				};
				$sInsert .= " `" .$fFields[$i] . "` ";
			};
			
			$sInsert .= " ) VALUES ( ";
			
			for($i=0;$i<=count($pValues)-1;$i++){
				if($i>0){
					$sInsert .= ", ";
				};
				$sInsert .= " '" . mysqli_real_escape_string($this->vConnection, $pValues[$fFields[$i]]) . "' ";
			};
			
			$sInsert .= " );";
			
			$qry = mysqli_query($this->vConnection, $sInsert);
			
			if(!($qry)){
				unset($sInsert);
				print_r(mysqli_error($this->vConnection));
				return false;
			};
			unset($sInsert);
			//mysql_free_result($qry);@DEPERCATED
			$result = mysqli_insert_id($this->vConnection);
			
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um Werte in der Datenbank zu updaten
		 * @return bool/int (affected_rows)
		 * @param string $pTable[optional]
		 * @param string $pIdx[optional]
		 * @param string $pIdxValue[optional]
		 * @param array $pValues[optional]
		 */
		function fDoUpdate($pTable = "", $pIdx = "", $pIdxValue = 0, $pValues = array()){
			$result = false;
			
			// verbindung herstellen
			$this->fConnect();
			
			if(($this->vConnection==0) or ($pTable=="") or ($pIdx=="") or ($pIdxValue===0) or (count($pValues)==0)){
			    print_r($this->vDieM);
				return $result;
			};
			
			$sUpdate = "";
			$sUpdate = "UPDATE `" . $pTable . "` SET ";
			
			$fFields = array_keys($pValues);
			
			for($i=0;$i<count($pValues);$i++){
				if($i>0){
					$sUpdate .= ", ";
				};
				$sUpdate .= " `" . $fFields[$i] . "`='" . mysqli_real_escape_string($this->vConnection, $pValues[$fFields[$i]]) . "' ";
			};
		
			$sUpdate .= " WHERE `" . $pIdx . "` ='" . mysqli_real_escape_string($this->vConnection, $pIdxValue) . "';";

			$qry = mysqli_query($this->vConnection, $sUpdate);
			
			if(!($qry)){
				unset($sUpdate);
				print_r(mysqli_error($this->vConnection));
				$this->fDisConnect();
				return $result;
			};
			
			unset($sUpdate);
			
			$result = mysqli_affected_rows($this->vConnection);
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um Werte aus der Datenbank zu lesen
		 * @return bool/array
		 * @param string $pSQL[optional]
		 * @param array $pOrderBy[optional]
		 * @param string $pLimit[optional]
		 */
		function fDoQuery($pSQL = "", $pOrderBy = array(), $pLimit = ''){
			$result = array();
			
			// verbindung herstellen
			$this->fConnect();
			
			if($this->vConnection==false){
				print_r($this->vDieM);
				$this->fDisConnect();
				return false;
			};
			$this->vSql = $pSQL;
			
			if(count($pOrderBy)>0){
				$this->vSql .= " ORDER BY ";
				for($o=0;$o<=count($pOrderBy)-1;$o++){
					if($o!=0){
						$this->vSql .= ", ";
					};
					$this->vSql .= $pOrderBy[$o][0] . " " . $pOrderBy[$o][1];
				};
			};
			
			if(($pLimit!='') and ($pLimit!=0)){
				$this->vSql .= " LIMIT " . $pLimit . ";";
			}else{
				$this->vSql .= ";";
			}

			$qry = mysqli_query($this->vConnection, $this->vSql);

			if(!($qry)){
				print_r(mysqli_error($this->vConnection));
				$this->fDisConnect();
				return false;
			};
			
			$c=0;
			while ($zQry = mysqli_fetch_array($qry)){
				//print_r($zQry);
				$fFields = array_keys($zQry);
				for($i=0;$i<=count($fFields)-1;$i++){
					if(bcmod($i,2)==1){
						$result[$c][$fFields[$i]]=$zQry[$fFields[$i]];
					};
				};
				$c++;
			};
			$this->vResult = $result;
			//mysqli_free_result($qry);
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $this->vResult;
		}
		
		/**
		 * Gibt alle Prozesse des Servers aus
		 * @return array
		 */
		function fListProcesses(){
			$result = false;
			
			// verbindung herstellen
			$this->fConnect();
			
			if($this->vConnection==0){
				print_r($this->vDieM);
				return $result;
			};

			$qry = mysqli_list_processes($this->vConnection);
			$counterProcesses = 0;
			$result = array();
			while($zQry = mysqli_fetch_assoc($qry)){
			    $result[$counterProcesses]= sprintf("%s %s %s %s %s", $zQry["Id"], $zQry["Host"], $zQry["db"], $zQry["Command"], $zQry["Time"]);
				$counterProcesses++;
			}
			//mysqli_free_result($qry);
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Server Status anzeigen
		 * @return string
		 */
		function fGetServerStat(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);

				return false;
			};
			
			$status = explode('  ', mysqli_stat($this->vConnection));
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $status;
		}
		
		/**
		 * Funktion, um Client Version anzuzeigen
		 * @return string
		 */
		function fGetClientVersion(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$info = mysqli_get_client_info();
			$result = "MySQL Client Version: <strong>" . $info . "</strong>";
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um Host Infos anzuzeigen
		 * @return string
		 */
		function fGetHostInfo(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$info = mysqli_get_host_info();
			$result = "MySQL Host Info: <strong>" . $info . "</strong>";
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um Protokoll Version anzuzeigen
		 * @return string
		 */
		function fGetProtoInfo(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$info = mysqli_get_proto_info();
			$result = "MySQL Protocol Version: <strong>" . $info . "</strong>";
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Client Encoding ausgeben
		 * @return string
		 */
		function fGetClientEncoding(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$result = mysqli_client_encoding();
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um Server Version anzuzeigen
		 * @return string
		 */
		function fGetServerVersion(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$info = mysqli_get_server_info();
			$result = "MySQL Server Version: <strong>" . $info . "</strong>";
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um die Verfügbaren Datenbanken des Benutzers anzuzeigen
		 * @return bool/array
		 */
		function fListDb(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};

			$result = array();
			
			$qry = mysqli_query("SHOW DATABASES");

			while ($row = mysqli_fetch_assoc($qry)) {
				$result[] = $row['Database'] . "\n";
			}
			
			// verbindung trennen
			$this->fDisConnect();

			return $result;
		}
		
		/**
		 * Funktion, um alle Tabellen der DB aufzulisten
		 * @return bool/array
		 */
		function fListTables(){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$result = array();
			
			$tTables = mysqli_list_tables($this->vDb);
				
			if (!$tTables) {
				print_r($this->vDieM);
				$this->fDisConnect();
				return false;
			}
			
			$r=0;
			while ($row = mysqli_fetch_row($tTables)) {
				$result[$r]=$row[0];
				$r++;
			}
			
			// verbindung trennen
			$this->fDisConnect();
			
			return $result;
		}
		
		/**
		 * Funktion, um die aktelle Datenbank zu wechseln.
		 * @return bool
		 * @param string $pDb[optional]
		 */
		function fSelectDb($pDb = ""){
			// verbindung herstellen
			$this->fConnect();
		    
		    if($this->vConnection==0){
				print_r($this->vDieM);
				return false;
			};
			
			$this->vDb = $pDb;
			if(mysqli_select_db($this->vConnection,$this->vDb)){
				$this->vConnected = 1;
				
				// verbindung trennen
				$this->fDisConnect();
				
				return true;
			}else{
				$this->vConnected = 0;
				print_r($this->vDieM);
				
				// verbindung trennen
				$this->fDisConnect();
				
				return false;
			};
		}
		
		/**
		 * Funktion, um das Resultat zu nullen
		 * @return bool
		 */
		function fUnsetResult(){
			$this->vResult=0;
			return true;
		}
		
		/**
		 * Funktion, um die Werte in einer Tabelle auszugeben
		 * @return string
		 */
		function fGetResultInTable(){
			if($this->vResult==0){
				return false;
			}
			
			if(count($this->vResult)==0){
				return false;
			}
			
			$htmlCode = "";
			$htmlCode.= "<table cellpadding=\"0\" cellspacing=\"2\" width=\"100%\" border=\"1\">\n";
			
			for($i=0;$i<=count($this->vResult)-1;$i++){
				if($i==0){
					$htmlCode .= "<tr>\n";
					$fFields=array_keys($this->vResult[$i]);
					for($f=0;$f<=count($fFields)-1;$f++){
						$htmlCode .= "<td><strong>" . $fFields[$f] . "</strong></td>\n";
					};
					$htmlCode .= "</tr>\n";
				};
				
				$htmlCode .= "<tr>\n";
				for($j=0;$j<=count($this->vResult[$i])-1;$j++){
					if($this->vResult[$i][$fFields[$j]]==""){
						$htmlCode .= "<td>&nbsp;</td>\n";
					}else{
						$htmlCode .= "<td>" . $this->vResult[$i][$fFields[$j]] . "</td>\n";
					};
				};
				$htmlCode .= "</tr>\n";
			};
			
			$htmlCode .= "</table>\n";
			
			return $htmlCode;
		}
		
		/**
		 * Funktion, um das Resultat in eine CSV Datei zu exportieren
		 * @return bool
		 * @param string $pCsvFileName[optional]
		 */
		function fWriteResultInCsv($pCsvFileName = ''){
			if($this->vResult==0){
				die('cSession::fWriteResultInCsv(): Result ist leer!');
			};
			
			if(count($this->vResult)==0){
				die('cSession::fWriteResultInCsv(): Result ist leer!');
			}
			
			if($pCsvFileName == ''){
				die('cSession::fWriteResultInCsv(): pCsvFileName ist leer!');
			};
			
			if(!is_dir(dirname($pCsvFileName) . '/')){
				die('cSession::fWriteResultInCsv(): Ordner existiert nicht!');
			}
			
			@$fp = fopen($pCsvFileName, 'w');
			if(false === $fp){
				$result = false;
			}else{
				for($i=0;$i<=count($this->vResult)-1;$i++){
					$tmpLineContent = '';
					if($i==0){
						$fFields=array_keys($this->vResult[$i]);
						for($f=0;$f<=count($fFields)-1;$f++){
							$tmpLineContent .= str_replace("\"", "\"\"", $fFields[$f]) . ";";
						};
						$tmpLineContent .= "\r\n";
					};
					
					for($j=0;$j<=count($this->vResult[$i])-1;$j++){
						if($this->vResult[$i][$fFields[$j]]==""){
							$tmpLineContent .= ";";
						}else{
							$tmpLineContent .= str_replace("\"", "\"\"", $this->vResult[$i][$fFields[$j]]) . ";";
						};
					};
					$tmpLineContent .= "\r\n";
					fputs($fp, $tmpLineContent);
				};
				
				// handle schliessen
				fclose($fp);
				$result = true;
			}

			return $result;
		}
	
		/**
		 * DB Dump schreiben
		 * @return bool
		 * @param string $pBackupName[optional]
		 */
		public function fGetDbDump($pBackupName = ''){
			$result = false;
			if($pBackupName == ''){
				return $result;
			}
			
			// wenn backup dir nicht existiert, dann false ausgeben
			if(!file_exists($this->vBackupDir) or !is_dir($this->vBackupDir)){
				return $result;
			}
			
			// wenn temp backup dir nicht existiert, dann false ausgeben
			if(!file_exists($this->vTempBackupDir) or !is_dir($this->vTempBackupDir)){
				return $result;
			}

			if(ini_get( 'max_execution_time' ) < $this->vExecutionTime) {
				set_time_limit($this->vExecutionTime);
			}
			
			// versuchen, ob das Logfile geoeffnet werden kann.
			$tmpLogFile = $this->vTempBackupDir . '/' . $pBackupName . '.log';
			$fpLog = @fopen($tmpLogFile, 'w');
			if($fpLog===false){
				return $result;
			}
			
			// Datei Header schreiben
			fwrite($fpLog, "Executing MySQL Backup Script v0.1\r\n");
			fwrite($fpLog, "Processing Databases...\r\n" );
			
			// schauen, ob es sich um windows oder unix handelt
			if(strtoupper(substr(PHP_OS, 0, 3))!=='WIN'){
				$tmpOs        = 'unix';
				$pBackupName .= '.tar';
			}else{
				$tmpOs        = 'windows';
				$pBackupName .= '.rar';
			}
			
			$this->vConnection = @mysqli_connect($this->vHost, $this->vUser, $this->vPass);
			if (!$this->vConnection){
				$this->vConnected = 0;
				print_r($this->vDieM);
				return false;
			};
			
			$tmpDbList = mysqli_list_dbs($this->vConnection);
			$dumpString = " --host=\"" . $this->vHost . "\" --user=\"" . $this->vUser . "\" --password=\"" . $this->vPass . "\"";
			$tmpExcludeList	= array();
			if( trim($this->vExcludeDb) != '' ) {
				$tmpExcludeList	= array_map('trim', explode(',', $this->vExcludeDb));
			}
			
			while($dbRow = mysqli_fetch_object($tmpDbList)){
				$dbName = $dbRow->Database;
	
				if(in_array($dbName, $tmpExcludeList)){
					// wenn die db nicht exportiert werden soll, zur naechsten springen
					continue;
				}
				
				unset($output);
				exec($this->vMySQLDir . "/mysqldump " . $dumpString . " --opt " . $dbName . " 2>&1 >" . $this->vTempBackupDir . "/" . $dbName . ".sql", $output, $res);
				if($res > 0){
					fwrite($fpLog, "DUMP FAILED\r\n" . implode( "\r\n", $output));
				}else{
					fwrite($fpLog, "Dumped DB: " . $dbName . "\r\n");
				}
			}
			//mysqli_free_result($tmpDbList);
			mysqli_close($this->vConnection);
			$this->vConnection = 0;
			$this->vConnected = 0;
			unset($tmpDbList, $dbName);
			
			fwrite($fpLog, "Archiving DBs...\r\n");
			chdir($this->vTempBackupDir);
			unset($output);
			if($tmpOs == 'unix') {
				exec("cd " . $this->vTempBackupDir . " ; nice -n 19 tar cf " . $this->vBackupDir . "/" . $pBackupName . " * 2>&1", $output, $res);
			}else{
				exec($this->vRarDir . "rar.exe a -ep -p" . $this->vPass . " " . $this->vBackupDir . "/" . $pBackupName . " " . $this->vTempBackupDir . "/*.sql 2>&1", $output, $res);
			}
			if ($res > 0) {
				fwrite($fpLog, "FAILED\r\n" . implode( "\r\n", $output));
			} else {
				fwrite($fpLog, "Set Password...\r\n");
				fwrite($fpLog, "Backup and Archiving completed successfully!\r\n");
				fwrite($fpLog, "Created File in " . $this->vBackupDir . "\r\n");
			}
			
			fclose($fpLog);
			
			// Die Logdatei in Backup Verzeichnis kopieren
			@copy($tmpLogFile, $this->vBackupDir . '/' . basename($tmpLogFile));
			
			// Dateien im Temp Backup Ordner loeschen
			if ($tmpDir = @opendir($this->vTempBackupDir)){
				while (($tmpFile = readdir($tmpDir))!==false){
					if (!is_dir($tmpFile)) {
						unlink($this->vTempBackupDir . '/' . $tmpFile);
					}
				}
			}
			closedir($tmpDir);
			unset($tmpDir, $tmpFile);
			
			// Temp Backup Ordner loeschen
			rmdir($this->vTempBackupDir);
			
			$result = true;
			return $result;
		}
		
		/**
		 * Setzt den MySQL Ordner
		 * @return void
		 * @param string $pMySQLDir[optional]
		 */
		public function mSetMySQLDir($pMySQLDir = ''){
			if($pMySQLDir == ''){
				die('cDb::mSetMySQLDir(): pMySQLDir ist leer!');
			}
			$this->vMySQLDir = $pMySQLDir;
		}
		
		/**
		 * Gibt den MySQL Ordner aus
		 * @return string
		 */
		public function mGetMySQLDir(){
			return $this->vMySQLDir;
		}
		
		/**
		 * Backup Ordner setzen
		 * @return void
		 * @param string $pBackupDir[optional]
		 */
		public function mSetBackupDir($pBackupDir = ''){
			if($pBackupDir == ''){
				die('cDb::mSetBackupDir(): pBackupDir ist leer!');
			}
			// überprüfen, ob das Verzeichnis existiert
			if(!file_exists($pBackupDir)){
				die('cDb::mSetBackupDir(): pBackupDir existiert nicht!');
			}
			
			if(!is_dir($pBackupDir)){
				die('cDb::mSetBackupDir(): pBackupDir ist kein Verzeichnis!');
			}
			$this->vBackupDir = $pBackupDir;
		}
		
		/**
		 * Backup Ordner ausgeben
		 * @return string
		 */
		public function mGetBackupDir(){
			return $this->vBackupDir;
		}
		
		/**
		 * Temp Backup Ordner eingeben
		 * @return void
		 * @param string $pTempBackupDir[optional]
		 */
		public function mSetTempBackupDir($pTempBackupDir = ''){
			if($pTempBackupDir == ''){
				die('cDb::mSetTempBackupDir(): pTempBackupDir ist leer!');
			}
			// überprüfen, ob das Verzeichnis existiert
			if(!file_exists($pTempBackupDir)){
				if(!@mkdir($pTempBackupDir)){
					die('cDb::mSetTempBackupDir(): pTempBackupDir konnte nich erstellt werden!');
				}
			}
			
			if(!is_dir($pTempBackupDir)){
				die('cDb::mSetTempBackupDir(): pTempBackupDir ist kein Verzeichnis!');
			}
			$this->vTempBackupDir = $pTempBackupDir;
		}
		
		/**
		 * Temp Backup Ordner ausgeben
		 * @return string
		 */
		public function mGetTempBackupDir(){
			return $this->vTempBackupDir;
		}
		
		/**
		 * DBs eintragen, die nicht ins Backup sollen
		 * @return void
		 * @param string $pExcludeDb[optional]
		 */
		public function mSetExcludeDb($pExcludeDb = ''){
			$this->vExcludeDb = $pExcludeDb;
		}
		
		/**
		 * DBs ausgeben, die nicht ins Backup sollen
		 * @return string
		 */
		public function mGetExcludeDb(){
			return $this->vExcludeDb;
		}
		
		/**
		 * Execution Time setzen
		 * @return void
		 * @param integer $pExecutionTime[optional]
		 */
		public function mSetExecutionTime($pExecutionTime =0){
			if(!is_int($pExecutionTime)){
				die('cDb::mSetExecutionTime(): pExecutionTime ist kein Integer!');
			}
			$this->vExecutionTime = $pExecutionTime;
		}
		
		/**
		 * Execution Time ausgeben
		 * @return integer
		 */
		public function mGetExecutionTime(){
			return $this->vExecutionTime;
		}
		
		/**
		 * Ordner setzen, in dem sich winrar befindet
		 * @return void
		 * @param string $pRarDir[optional]
		 */
		public function mSetRarDir($pRarDir = ''){
			if(!is_dir($pRarDir)){
				die('cDb::mSetRarDir(): pRarDir ist kein Verzeichnis!');
			}
			$this->vRarDir = $pRarDir;
		}
		
		/**
		 * Ordner ausgeben, in dem sich winrar befindet
		 * @return string
		 */
		public function mGetRarDir(){
			return $this->vRarDir;
		}
	}
?>