<div class="col-md-12" id="leitfaden">
    <?php if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) { ?>
        <h3>Chère / Cher auteur,</h3>
        <p>Vous trouverez ici notre notice pour la rédaction des articles dans notre revue spécialisée FAÇADE. Cette notice contient les informations pertinentes concernant la structure des articles dans les rubriques Reportage et Technique, afin de vous faciliter la rédaction. N'hésitez pas à nous contacter pour toute autre question : <a href="mailto:info@szff.ch">info@szff.ch</a></p>
        <p><br /><br />
        <a href="/media/mediadaten/Leitfaden-Artikelerstellung-f.pdf" target="_blank" class="btn btn-abo">Téléchargement</a></p>
   

    <?php } 
          else { ?>
		<h3>Liebe Autorin, lieber Autor</h3>
        <p>Hier finden Sie unseren Leitfaden für die Artikel-Erstellung in unserem Fachmagazin FASSADE. Darin sind wichtige Informationen zum Artikelaufbau in den Rubriken Report oder Technik zusammengefasst, welche die Erstellung eines Artikels erleichtern sollen. Für weitere Fragen stehen wir Ihnen gerne zur Verfügung: <a href="mailto:info@szff.ch">info@szff.ch</a></p>
        
        <p><br /><br />    
        <a href="/media/mediadaten/Leitfaden-Artikelerstellung-d.pdf" target="_blank" class="btn btn-abo">Download</a></p>    

    <?php } ?>
    <a href="/" class="btn backBtn"><?php echo __('zurueck');?></a>
</div>