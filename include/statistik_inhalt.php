<?php
if (isset($_POST['ajaxCall'])) {
    session_start();
    require_once("../config.php");
}
// datum init
$statFrom = 0;
$statTo   = 0;

if($statFrom == 0) $statFrom = mktime(0, 0, 0, date('m'), date('d')-30, date('Y'));
if($statTo == 0) $statTo = mktime(23, 59, 59, date('m'), date('d'), date('Y'));

// daten in session speichern, wenn gesetzt
if(isset($_POST['btn_setFilterStat'])){
	// datum validieren und in timestamp umwandeln!
	$tmpStatFrom = $_POST['statFrom'];
	$tmpStatFrom = explode('.', $tmpStatFrom);
	$tmpStatFrom = mktime(0, 0, 0, $tmpStatFrom[1], $tmpStatFrom[0], $tmpStatFrom[2]);
	
	if($tmpStatFrom){		
		$statFrom = $tmpStatFrom;
		$_SESSION['forumStatFrom'] = $statFrom;
	}
	
	$tmpStatTo = $_POST['statTo'];
	$tmpStatTo = explode('.', $tmpStatTo);
	$tmpStatTo = mktime(23, 59, 59, $tmpStatTo[1], $tmpStatTo[0], $tmpStatTo[2]);
	
	if($tmpStatTo){		
		$statTo = $tmpStatTo;
		$_SESSION['forumStatTo'] = $statTo;
	}
}

// verbindung herstellen
$dbConn = mysqli_connect(_HOST, _USER, _PW);
mysqli_select_db($dbConn, _DB);
mysqli_query($dbConn, "SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");			

$sqlTop20De = "SELECT fld_keyword, COUNT(idx_statistik) AS anz 
               FROM tbl_statistik 
			   WHERE fld_lang=0 AND fld_keyword!=''
			   AND fld_time>'" . $statFrom . "' AND fld_time<='" . $statTo . "'
			   GROUP BY fld_keyword ORDER BY anz DESC LIMIT 0,20";	
$qryTop20De = mysqli_query($dbConn, $sqlTop20De);
if(!($qryTop20De)){
	return mysqli_error($dbConn);
}
$aTop20De = array();
$tmpCount = 0;
while ($zQry = mysqli_fetch_array($qryTop20De)){
	$aTop20De[$tmpCount]['keyword'] = $zQry['fld_keyword'];
	$aTop20De[$tmpCount]['anz']     = $zQry['anz']        ;
	$tmpCount++;
}
unset($sqlTop20De, $qryTop20De, $tmpCount);

$sqlTop20Fr = "SELECT fld_keyword, COUNT(idx_statistik) AS anz 
               FROM tbl_statistik 
			   WHERE fld_lang=1 AND fld_keyword!=''
			   AND fld_time>'" . $statFrom . "' AND fld_time<='" . $statTo . "'
			   GROUP BY fld_keyword ORDER BY anz DESC LIMIT 0,20";	
$qryTop20Fr = mysqli_query($dbConn, $sqlTop20Fr);
if(!($qryTop20Fr)){
	return mysqli_error($dbConn);
}
$aTop20Fr = array();
$tmpCount = 0;
while ($zQry = mysqli_fetch_array($qryTop20Fr)){
	$aTop20Fr[$tmpCount]['keyword'] = $zQry['fld_keyword'];
	$aTop20Fr[$tmpCount]['anz']     = $zQry['anz']        ;
	$tmpCount++;
}
unset($sqlTop20Fr, $qryTop20Fr, $tmpCount);

$sqlHitsDe = "SELECT COUNT(idx_statistik) AS anz
              FROM tbl_statistik 
			  WHERE fld_lang=0
			  AND fld_time>'" . $statFrom . "' AND fld_time<='" . $statTo . "'";	
$qryHitsDe = mysqli_query($dbConn, $sqlHitsDe);
if(!($qryHitsDe)){
	return mysqli_error($dbConn);
}
$aHitsDe = array();
while ($zQry = mysqli_fetch_array($qryHitsDe)){
	$aHitsDe[0]['anz'] = $zQry['anz'];
}
unset($sqlHitsDe, $qryHitsDe);

$sqlHitsFr = "SELECT COUNT(idx_statistik) AS anz 
              FROM tbl_statistik
			  WHERE fld_lang=1
			  AND fld_time>'" . $statFrom . "' AND fld_time<='" . $statTo . "'";	
$qryHitsFr = mysqli_query($dbConn, $sqlHitsFr);
if(!($qryHitsFr)){
	return mysqli_error($dbConn);
}
$aHitsFr = array();
while ($zQry = mysqli_fetch_array($qryHitsFr)){
	$aHitsFr[0]['anz'] = $zQry['anz'];
}
unset($sqlHitsFr, $qryHitsFr);

if (!isset($_POST['ajaxCall'])) {
?>
<form action="" id="fassaden_archiv" name="fassaden_archiv" enctype="multipart/form-data" method="post" onsubmit="">
<div class="row align-items-end">
<div class="col">
	<div class="form-group mb-2">
    	<label for="statFrom"><?php echo __('filterFrom'); ?> *</label>
		<input type="text" name="statFrom" id="statFrom" size="20" value="<?php echo date('d.m.Y', $statFrom); ?>" class="form-control" data-date="<?php echo $statFrom; ?>">
	</div>
</div>
<div class="col">
	<div class="form-group mb-2 mx-sm-3">
		<label for="statTo"><?php echo __('filterTo'); ?> *</label>
		<input type="text" name="statTo" id="statTo" size="20" value="<?php echo date('d.m.Y', $statTo); ?>" class="form-control" data-date="<?php echo $statTo; ?>">
	</div>
</div>
<div class="col">
	<button type="submit" name="btn_setFilterStat" id="btn_setFilterStat" class="btn mb-2"><?php echo __('setFilter'); ?></button>
</div>
</div>
</form>
<?php 
}
?>
<div class="statErgebnis">
<h3><?php echo __('stat_title_keywords_de'); ?></h3>
<?php if(count($aTop20De)): ?>
	<table>
		<tbody>
			<?php for($i=0; $i<count($aTop20De); $i++): ?>
				<tr>
					<td class="stat_keywort_td"><?php echo utf8_decode($aTop20De[$i]['keyword']); ?></td>
					<td><strong><?php echo $aTop20De[$i]['anz']; ?></strong></td>
				</tr>
			<?php endfor; ?>
		</tbody>
	</table>
<?php else: ?>
	<p><?php echo __('stat_no_entries'); ?></p>
<?php endif; ?>
<p>&nbsp;</p>
<h3><?php echo __('stat_title_keywords_fr'); ?></h3>
<?php if(count($aTop20Fr)): ?>
	<table>
		<tbody>
			<?php for($i=0; $i<count($aTop20Fr); $i++): ?>
				<tr>
					<td class="stat_keywort_td"><?php echo utf8_decode($aTop20Fr[$i]['keyword']); ?></td>
					<td><strong><?php echo $aTop20Fr[$i]['anz']; ?></strong></td>
				</tr>
			<?php endfor; ?>
		</tbody>
	</table>
<?php else: ?>
	<p><?php echo __('stat_no_entries'); ?></p>
<?php endif; ?>
<p>&nbsp;</p>
<h3><?php echo __('stat_title_anz_hits_de'); ?></h3>
<?php if(count($aHitsDe)): ?>
	<p><strong><?php echo $aHitsDe[0]['anz']; ?></strong></p>
<?php else: ?>
	<p><?php echo __('stat_no_entries'); ?></p>
<?php endif; ?>
<p>&nbsp;</p>
<h3><?php echo __('stat_title_anz_hits_fr'); ?></h3>
<?php if(count($aHitsFr)): ?>
	<p><strong><?php echo $aHitsFr[0]['anz']; ?></strong></p>
<?php else: ?>
	<p><?php echo __('stat_no_entries'); ?></p>
<?php endif; ?>
</div>