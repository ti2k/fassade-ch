<?php 
session_start();
require_once("../config.php");
http_response_code(200);

if (isset($_POST['setAusgabe']) && isset($_POST['data'])) {
    $data = json_decode($_POST['data']);
    $_SESSION['searchKeyword'] = trim($data->keyword);
    $_SESSION['searchYear'] = intval($data->setJahr);
    $_SESSION['searchMonth'] = intval($data->setMonat);
    $return = array('control' => 'ok', 'loggedIn' => ((isset($_SESSION['login']) && ($_SESSION['login'] == 1))? true: false));
    echo json_encode($return);
    die();
}

//Überprüft ob die Get-Variable "logout" gesetzt ist und ob sie den Wert 1 hat, wenn dies der Fall ist soll die Session gelöscht werden
//Wenn diese Abfrage stimmt wird der Benutzer ausgeloggt
if(isset($_GET['logout']) && $_GET['logout'] == 1){
    $_SESSION['login'] = 0;
    unset($_SESSION['admin']);
    session_destroy();
    die();
}

if (isset($_POST['passwort'])) {
    //Adminpasswort auslesen und überprüfen
    if(isset($_POST['passwort'])){
        $queryAdminLogin = "SELECT * FROM `tbl_passwort` WHERE fld_passwort = '" .  md5($_POST['passwort']) . "' AND fld_isAdmin = 1";
        $sqlAdminLogin = $sql->fDoQuery($queryAdminLogin);
    }
    
    //Überprüft ob ein Eintrag bei der SQL abfrage ausgelesen wurde
    if(isset($sqlAdminLogin) && $sqlAdminLogin != "" && count($sqlAdminLogin) > 0){
        $_SESSION['login'] = 1;
        $_SESSION['admin'] = 1;
        die();
    }
    
    // Monatslogin auslesen und überprüfen
    $timestamp = time();
    if(isset($_POST['passwort'])){
        $queryLogin = "SELECT * FROM `tbl_passwort` WHERE fld_passwort = '" .  md5($_POST['passwort']) . "' AND fld_isAdmin = 0 AND '" . $timestamp . "' >= fld_start AND '" . $timestamp . "' <= fld_end";
        $sqlLogin = $sql->fDoQuery($queryLogin);
    }
    
    //Überprüft ob ein Eintrag bei der SQL abfrage ausgelesen wurde
    if(isset($sqlLogin) && $sqlLogin != "" && count($sqlLogin) > 0){
        $_SESSION['login'] = 1;
        die();
    }
    else {
        echo md5($_POST['passwort']);
    }
}
http_response_code(400);
?>