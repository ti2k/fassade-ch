<div class="col-md-12" id="abo">
    <h1>Abonnement</h1>
    <?php if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) { ?>
        <p>4 numéros par an</p>
        <h3>Prix de l’abonnement</h3>
        <p>Suisse : CHF 80.00<br />
        Etranger : CHF 110.00<br />
        Prix à l’unité : CHF 20.00 (envoi uniquement en Suisse)<br /><br />
        (TVA et frais d’envoi inclus)<br /><br />
        <a href="mailto:fassade@szff.ch?Subject=Abonnement FAÇADE" class="btn btn-abo">Commander maintenant</a></p>
   

    <?php } 
          else { ?>
         <p>4 Ausgaben jährlich</p>
        
        <h3>Abonnementspreis</h3>
        <p>Schweiz: CHF 80.00<br />
        Ausland: CHF 110.00<br />
        Einzelpreis: CHF 20.00 (Versand nur innerhalb der Schweiz)<br /><br />    
        (Preise inklusiv MWST. und Versand)<br /><br />    
        <a href="mailto:fassade@szff.ch?Subject=FASSADE Abonnement" class="btn btn-abo">Jetzt bestellen</a></p>    

    <?php } ?>
    <a href="/" class="btn backBtn"><?php echo __('zurueck');?></a>
</div>