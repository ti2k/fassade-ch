<?php
if (isset($_GET['ajaxCall'])) {
    session_start();
    require_once("../config.php");
}

// Select Befehl das alle Jahre nur einmal ausgegeben werden (z.B. nicht 2 mal 2011 im  DropDown)
$sqlDefineYear = "SELECT DISTINCT fld_jahr FROM tbl_archiv";
$queryDefineYear = $sql->fDoQuery($sqlDefineYear);

// Select Befehl das alle Monate nur einmal ausgegeben werden (z.B. nicht 2 mal 06 im  DropDown)
$sqlDefineMonth= "SELECT DISTINCT fld_monat FROM tbl_archiv";
$queryDefineMonth = $sql->fDoQuery($sqlDefineMonth);

//Überprüft ob überhaubt eine Suche getätigt wurde
if(isset($_POST['keyword']) || isset($_POST['select_jahr']) || isset($_POST['select_monat'])){
	//Überprüft ob ein Suchwort(keyword) eingegebn wurde, wenn ja soll das in die DB geschrieben werden ansonsten nichts
	if(isset($_POST['keyword'])){
		$statistikKeyword = $_POST['keyword'];
	}else{
		$statistikKeyword = NULL;
	}
	
	//Immer den Aktuellen Timestamp speichern
	$aktuelleZeit = time();
	
	//Suchwort und Timestamp in ein Array schrieben
	$sqlEinfuegenStatistik = array("fld_keyword" => $statistikKeyword,"fld_lang"=> $_SESSION['language'],"fld_time" => $aktuelleZeit);
	$sql->fDoInsert("tbl_statistik", $sqlEinfuegenStatistik);
}

// daten in session speichern, wenn gesetzt
if(isset($_POST['keyword'])){
	$_SESSION['searchKeyword'] = trim($_POST['keyword']);
}
if(isset($_POST['select_jahr'])){
	$_SESSION['searchYear'] = intval($_POST['select_jahr']);
}
if(isset($_POST['select_monat'])){
	$_SESSION['searchMonth'] = intval($_POST['select_monat']);
}
if(isset($_GET['keyword']) && isset($_GET['select_jahr']) && isset($_GET['select_monat'])){
    $_SESSION['searchKeyword'] = trim($_GET['keyword']);
    $_SESSION['searchYear'] = intval($_GET['select_jahr']);
    $_SESSION['searchMonth'] = intval($_GET['select_monat']);
}
?>

<div id="suchformular">
	<form method="post" action="#archiv" enctype="multipart/form-data">
		<div class="form-group">
    		<input id="keyword" class="form-control" placeholder="<?php echo __('keyword'); ?> *" type="text" value="<?php echo (isset($_SESSION['searchKeyword'])? $_SESSION['searchKeyword']: ''); ?>" name="keyword">
		</div>
		<div class="form-group">
			<label for="select_jahr"><?php echo __('year'); ?></label>
			<select id="select_jahr" name='select_jahr' class='form-control'> 
				<option value="0" <?php if(!isset($_SESSION['searchYear'])){ ?> selected="selected"<?php }; ?>><?php echo __('pleaseChose'); ?></option>
				<?php
				sort($queryDefineYear);
				for($a=0; $a <= count($queryDefineYear); $a++){
					foreach ($queryDefineYear[$a] as $key => $val) {
					  echo '<option value= "'.$val.'"'. ((isset($_SESSION['searchYear']) && $val == $_SESSION['searchYear'])? ' selected="selected"': '').'> '.$val.'</option>';
					}
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="select_monat"><?php echo __('month'); ?></label>
			<select id="select_monat" name='select_monat' class='form-control'> 
				<option value="0" <?php if(!isset($_SESSION['searchMonth'])){ ?> selected="selected"<?php }; ?>><?php echo __('pleaseChose'); ?></option>
				<?php
				sort($queryDefineMonth);
				for($i=0; $i <= count($queryDefineMonth); $i++){
					foreach ($queryDefineMonth[$i] as $key => $val) {
					  echo '<option value= "'.$val.'"'. ((isset($_SESSION['searchMonth']) && $val == $_SESSION['searchMonth'])? ' selected="selected"': '').'> '.$val.'</option>';
					}
				}
				?>
			</select>
		</div>
		<?php echo '<button class="btn btn-default" type="submit">' . __('search') . '</button>';
		if(isset($_SESSION['login']) && $_SESSION['login'] != 0 ){
    		echo '<a class="btn btn-logout float-right" href="?logout=1#archiv" id="logout">Logout</a>';
    	}
    	?>
	</form>
</div>
<hr>
