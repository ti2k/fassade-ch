<?php
if (isset($_GET['ajaxCall'])) {
    session_start();
    require_once("../config.php");
}
$searchOperator = 'and';

//ob ein PDF gefunden wurde (bei 0 wurden PDF's gefunden bei 1 nicht)
$noPDF = 0;

// abfrage zusammensetzen
$sqlAll = "SELECT *
		 	FROM `tbl_archiv`";
		 
$tmpWhere = "";

// sprache
if(!isset($_SESSION['language']) || $_SESSION['language']  == 0){
	$tmpWhere .= "`fld_artikelname`!=''";
}else{
	$tmpWhere .= "`fld_artikelname_fr`!=''";
}

// jahr
if((isset($_SESSION['searchYear'])) && (intval($_SESSION['searchYear'])!=0)){
	if(trim($tmpWhere)!='') $tmpWhere .= " AND ";
	$tmpWhere .= "`fld_jahr` ='" . $_SESSION['searchYear'] . "'";	
}

// monat
if((isset($_SESSION['searchMonth'])) && (intval($_SESSION['searchMonth'])!=0)){
	if(trim($tmpWhere)!='') $tmpWhere .= " AND ";
	$tmpWhere .= "`fld_monat` ='" . $_SESSION['searchMonth'] . "'";	
}

// keywords
$keyWordsSplitted = array();
if(isset($_SESSION['searchKeyword']) && trim($_SESSION['searchKeyword'])!=''){
	$searchKeyword = utf8_decode($_SESSION['searchKeyword']);
	if($searchOperator == 'whole'){
		// nach der wortkette suchen
		if(trim($tmpWhere)!='') $tmpWhere .= " AND ";
		
		$tmpWhere .= "`fld_txt` LIKE '%" . $searchKeyword . "%'";
	}else{
		// begriffe teilen
		$keyWordsSplitted = explode(' ', $searchKeyword);
		
		// fr alle teilbegriffe ein like %% abfrage
		for($i=0; $i<count($keyWordsSplitted); $i++){
			if(!$i and trim($tmpWhere) != '') $tmpWhere .= " AND ";
			if(!$i) $tmpWhere .= " ( ";
			
			if($i){					
				if($searchOperator == 'or'){
					$tmpWhere .= " OR ";
				}else{
					$tmpWhere .= " AND ";
				}
				
			}
			$tmpWhere .= "`fld_txt` LIKE '%" . $keyWordsSplitted[$i] . "%'";	
			
			if($i == count($keyWordsSplitted)-1) $tmpWhere .= " ) ";
		}
	}
}	

if(trim($tmpWhere)!='') $tmpWhere = " WHERE " . $tmpWhere;	
$sqlAll .= $tmpWhere . ' ORDER BY fld_jahr desc, fld_monat desc, fld_artikelname asc';

// überprüfen, ob suchkriterium gesetzt
if(isset($_SESSION['searchKeyword']) && trim($_SESSION['searchKeyword']) != '' or isset($_SESSION['searchYear']) && intval($_SESSION['searchYear'])>0 or isset($_SESSION['searchMonth']) && intval($_SESSION['searchMonth'])>0){
	// wenn ja, resultate auslesen
	$qryAll = $sql->fDoQuery($sqlAll);
}else{
	// wenn nicht, keine resultate ausgeben
	$qryAll = array();
}


/*//Anzahl aller gesuchten Einträge anzeigen 
$anzahlEintraegeTotal = count($qryAll);*/
	
	
?>

<div id="list">
	<h2><?php //echo $anzahlEintraegeTotal; ?></h2>
	<?php if(count($qryAll)): ?>
		<h3><?php echo sprintf(__('results'), count($qryAll)); ?></h3>
		<ul>
		<?php
			for($i = 0; $i < count($qryAll); $i++){				
				// den resulttext formatiert ausgeben mit den resultaten...
				$tmpText = '';
				
				$tmpArtikelname = trim(($qryAll[$i]['fld_artikelname']));
				$tmpArtikeltitel = trim(($qryAll[$i]['fld_titel']));
				$tmpArtikelteaser = trim(($qryAll[$i]['fld_teaser']));	
				$tmpPdf = trim($qryAll[$i]['fld_pdf']);						
				if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1){
					if(trim($qryAll[$i]['fld_artikelname_fr'])!=''){
						$tmpArtikelname = trim(($qryAll[$i]['fld_artikelname_fr']));
					}else{
						$tmpArtikelname = '';
					}
					
					if(trim($qryAll[$i]['fld_titel_fr'])!=''){
						$tmpArtikeltitel = trim(($qryAll[$i]['fld_titel_fr']));
					}else{
						$tmpArtikeltitel = '';
					}
					
					if(trim($qryAll[$i]['fld_teaser_fr'])!=''){
						$tmpArtikelteaser = trim(($qryAll[$i]['fld_teaser_fr']));
					}else{
						$tmpArtikelteaser = '';
					}
					
					if(trim($qryAll[$i]['fld_pdf_fr'])!=''){
						$tmpPdf = trim($qryAll[$i]['fld_pdf_fr']);
					}						
				}		
				?>
				<li>
					<p>
						<?php	
							// PDF-Link Definieren
							$jahr =  ($qryAll[$i]["fld_jahr"]);
							$monat = ($qryAll[$i]["fld_monat"]);
							
							if((int)($_SESSION['searchMonth']) < 10){
								$monat = "0" . utf8_encode($qryAll[$i]["fld_monat"]);
							}
							
							$tmpPDFLink = _URL_PDF . $jahr . "/" . $monat . "/" . $tmpPdf;
						?>
						<a id="pdf_link" target="_blank" href="<?php echo $tmpPDFLink;  ?>"><i class="far fa-file-pdf"></i> <?php echo ($qryAll[$i]["fld_jahr"]) . "/" . ($qryAll[$i]["fld_monat"]) . " " . $tmpArtikelname; ?></a><br/><strong><?php echo $tmpArtikeltitel; ?></strong> - <?php echo mb_substr($tmpArtikelteaser, 0, 200); ?>...
					</p>							
				</li>
				<?php
			}
			?>
			</ul>
		<?php else: ?>
				<p class='fehlerMeldung'><?php echo __('noResults'); ?></p>
		<?php endif; ?>
</div>
