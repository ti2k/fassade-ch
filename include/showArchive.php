<div id="content-middle" class="col-md-12">
	<div id="content-middle-top">
		<?php
			//Wenn die Session Login nicht gesetzt ist oder Sie den Wert 0 hat, soll die login.php Datei includet werden, ansonsten die suchformular.php Datei
			if(!isset($_SESSION['login']) || $_SESSION['login'] == 0){
			    if(isset($_GET['keyword']) && isset($_GET['select_jahr']) && isset($_GET['select_monat'])){
			        $_SESSION['searchKeyword'] = trim($_GET['keyword']);
			        $_SESSION['searchYear'] = intval($_GET['select_jahr']);
			        $_SESSION['searchMonth'] = intval($_GET['select_monat']);
			    }
				include("include/login.php");
			}else{
				if(isset($_GET['statistik']) && $_GET['statistik'] == 1){
					include("include/statistik.php");
				}else{
					include("include/suchformular.php");
				}
			}
		?>
	</div>
	<div id="content-middle-bottom">
		<?php 
			if((isset($_SESSION['login']) && $_SESSION['login'] == 1 && !isset($_GET['statistik'])) || (isset($_SESSION['login']) && $_SESSION['login'] == 1 && isset($_GET['statistik']) && $_GET['statistik'] != 1)):
				include("include/showPDF.php"); 
			endif;
		?>
	</div>			
</div>
<?php
	//Ist die Session "login" gesetzt und man somit eingeloggt ist, erscheint ein Link zur Statistik seite
	if((isset($_SESSION['login']) && $_SESSION['login'] != 0) && (isset($_SESSION['admin']) && $_SESSION['admin'] != 0)){
		if(isset($_GET['statistik']) && $_GET['statistik'] == 1){
			echo '<a class="btn statLink" href="?statistik=0#archiv" id="suchen_link">' . __('search') . '</a>';	
		}else{
			echo '<a class="btn statLink" href="?statistik=1#archiv" id="statistik_link">' . __('statistik') . '</a>';	
		}
	}
	
	//Ist die Session "login" gesetzt und ihr Wert ist nicht gleich 0 dan soll der logout Button erscheinen
	if(isset($_SESSION['login']) && $_SESSION['login'] != 0 ){
		echo '<a class="btn btn-logout d-none d-md-block" href="?logout=1#archiv" id="logout">Logout</a>';
	}
?>