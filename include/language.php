<?php	
	# sprachausgabe
	function __($pKey = ''){
		# alle bersetzungen	
		$lang = array();
		
		# DE
		$lang[0]['keyword'] = 'Stichworte:';
		$lang[0]['year'] = 'Jahr:';
		$lang[0]['month'] = 'Heft:';
		$lang[0]['search'] = 'Suchen';
		$lang[0]['password'] = 'Passwort';
		$lang[0]['results'] = 'Resultate: %s Dokument(e)';
		$lang[0]['pleaseChose'] = 'Auswahl';
		$lang[0]['noResults'] = 'Keine Eintr&auml;ge gefunden';
		$lang[0]['and'] = 'UND';
		$lang[0]['or'] = 'ODER';
		$lang[0]['wholeWord'] = 'Ganzes Wort';
		$lang[0]['noDecision'] = 'Bitte treffen Sie eine Auswahl';
		$lang[0]['hint_keywords'] = 'Die Stichworte m&uuml;ssen mit einem Leerzeichen getrennt werden. Es werden nur die Resultate angezeigt, die alle Suchbegriffe enthalten. Die Ergebnisse sind nach Anzahl Vorkommnisse der Suchbegriffe im Dokument geordnet (Relevanz, als Prozentzahl in Klammern angezeigt).';
		$lang[0]['stat_title_keywords_de'] = 'Top 20 Suchbegriffe DE';
		$lang[0]['stat_title_keywords_fr'] = 'Top 20 Suchbegriffe FR';
		$lang[0]['stat_title_anz_hits_de'] = 'Anzahl Suchen DE';
		$lang[0]['stat_title_anz_hits_fr'] = 'Anzahl Suchen FR';
		$lang[0]['stat_no_entries'] = 'Keine Eintr&auml;ge';
		$lang[0]['setFilter'] = 'Filter setzen';
		$lang[0]['filterFrom'] = 'Von Datum';
		$lang[0]['filterTo'] = 'Bis Datum';
		
		$lang[0]['aktuelleAusgabe'] = 'Aktuelle Ausgabe';
		$lang[0]['aktuelleAusgabeSubline'] = 'Fenster- und Fassadentechnik unter die Lupe genommen – von Profis für Profis.';
		$lang[0]['probenummer'] = 'Probenummer anfordern';
		$lang[0]['artikelbibliothek'] = 'Artikelbibliothek';
		$lang[0]['probenummer_subject'] = 'FASSADE: Probenummer bestellen';
		$lang[0]['technik'] = 'Technik';
		$lang[0]['report'] = 'Report';
		$lang[0]['fruehereAusgaben'] = 'Frühere Ausgaben';
		$lang[0]['passwort1'] = 'Bitte geben Sie Ihr Passwort ein';
		$lang[0]['passwort2'] = 'Sie finden den Login auf der letzten Seite der aktuellen Ausgabe «FASSADE».';
		$lang[0]['pw_small1'] = 'Sie haben noch kein Login?';
		$lang[0]['pw_small2'] = 'Jetzt gleich anfordern unter';
		$lang[0]['pw_subject'] = 'Login für FASSADE Homepage';
		$lang[0]['logout_success'] = 'Sie wurden erfolgreich ausgeloggt!';
		$lang[0]['bitte_einloggen'] = 'Bitte loggen Sie sich ein, um den gewünschten Inhalt zu sehen.';
		$lang[0]['login_abgelaufen'] = 'Ihre Logindaten sind nicht korrekt oder abgelaufen.';
		$lang[0]['mediadaten'] = 'Mediadaten';
		$lang[0]['download_pdf'] = 'Download Mediadaten als PDF';
		$lang[0]['onlineshop'] = 'Onlineshop';
		$lang[0]['szff'] = 'SZFF';
		$lang[0]['impressum'] = 'Impressum/Kontakt';
		$lang[0]['datenschutz'] = 'Datenschutz';
		$lang[0]['footer'] = '<strong>Herausgeber, Redaktion und Verlag:</strong><br />SZFF/CSFF<br />Schweizerische Zentrale Fenster und Fassaden<br />Ringstrasse 15, CH-4600 Olten<br />www.szff.ch';
		$lang[0]['zurueck'] = 'zurück';
		$lang[0]['statistik'] = 'Statistik';
		$lang[0]['leitfaden'] = 'Leitfaden für Autoren';
			
		# FR
		$lang[1]['keyword'] = 'Mots cl&eacute;s:';
		$lang[1]['year'] = 'Ann&eacute;e:';
		$lang[1]['month'] = 'Revue:';
		$lang[1]['search'] = 'Chercher';
		$lang[1]['password'] = 'Mot de passe';
		$lang[1]['results'] = 'R&eacute;sultats: %s document(s)';
		$lang[1]['pleaseChose'] = 'Sélection';
		$lang[1]['noResults'] = 'Pas d\'entr&eacute;es trouv&eacute;';
		$lang[1]['and'] = 'avec';
		$lang[1]['or'] = 'ou';
		$lang[1]['wholeWord'] = 'mot entier';
		$lang[1]['noDecision'] = 'Veuillez faire un choix';
		$lang[1]['hint_keywords'] = 'Les mots-cl&eacute;s doivent &ecirc;tre s&eacute;par&eacute;s par un espace. Les r&eacute;sultats affich&eacute;s correspondent aux termes de recherche. Les r&eacute;sultats sont class&eacute;s en fonction du nombre d\'occurrences des termes de recherche dans le document (pertinence affich&eacute;e en pourcentage entre parenth&egrave;ses).';
		$lang[1]['filterFrom'] = 'Von Datum';
		$lang[1]['filterTo'] = 'Bis Datum';
		$lang[1]['stat_title_keywords_de'] = 'Top 20 Suchbegriffe DE';
		$lang[1]['stat_title_keywords_fr'] = 'Top 20 Suchbegriffe FR';
		$lang[1]['stat_title_anz_hits_de'] = 'Anzahl Suchen DE';
		$lang[1]['stat_title_anz_hits_fr'] = 'Anzahl Suchen FR';
		
		$lang[1]['aktuelleAusgabe'] = 'Numéro actuel';
		$lang[1]['aktuelleAusgabeSubline'] = 'La technique des fenêtres et façades passée au crible – par les pros, pour les pros.';
		$lang[1]['probenummer'] = 'Demander un numéro à l’essai';
		$lang[1]['artikelbibliothek'] = 'Archives d’articles';
		$lang[1]['probenummer_subject'] = 'FAÇADE : commander un numéro à l’essai';
		$lang[1]['technik'] = 'Technique';
		$lang[1]['report'] = 'Reportage';
		$lang[1]['fruehereAusgaben'] = 'Numéros précédents';
		$lang[1]['passwort1'] = 'Veuillez saisir votre mot de passe';
		$lang[1]['passwort2'] = 'Vous trouverez le mot de passe sur la dernière page du numéro actuel de la «FAÇADE».';
		$lang[1]['pw_small1'] = 'Vous n’avez pas encore de login?';
		$lang[1]['pw_small2'] = 'Demandez-le maintenant sous';
		$lang[1]['pw_subject'] = 'Login de FAÇADE Homepage';
		$lang[1]['logout_success'] = 'Vous vous êtes déconnecté avec succès !';
		$lang[1]['bitte_einloggen'] = 'Veuillez-vous connecter pour consulter le contenu désiré.';
		$lang[1]['login_abgelaufen'] = 'Vos données de connexion ne sont pas valides ou expirées.';
		$lang[1]['mediadaten'] = 'Données médias';
		$lang[1]['download_pdf'] = 'Télécharger les données médias au format PDF';
		$lang[1]['onlineshop'] = 'Shop en ligne';
		$lang[1]['szff'] = 'CSFF';
		$lang[1]['impressum'] = 'Ours/contact';
		$lang[1]['datenschutz'] = 'Protection des données';
		$lang[1]['footer'] = '<strong>Editeur, rédaction et maison d’édition:</strong><br />SZFF/CSFF<br />Centrale Suisse Fenêtres et Façades<br />Ringstrasse 15, CH-4600 Olten<br />www.szff.ch';
		$lang[1]['zurueck'] = 'retour';
		$lang[1]['statistik'] = 'Statistiques';
		$lang[1]['leitfaden'] = 'Notice pour les auteurs';

		
		
		if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1 && isset($lang[1][$pKey])){
			return $lang[1][$pKey];
		}else{
			return $lang[0][$pKey];
		}
		
		return $pKey;
	}
?>