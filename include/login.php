<?php
if (isset($_GET['ajaxCall'])) {
    session_start();
    require_once("../config.php");
}
?>
<div class="row justify-content-center">
    <div class="col-md-6 text-center">
    	<?php 
    	if (isset($_GET['logout'])) {
    	    echo '<div class="alert alert-success" role="alert">'.__('logout_success').'</div>';
    	}
    	if(isset($_GET['keyword']) && isset($_GET['select_jahr']) && isset($_GET['select_monat'])) {
    	    echo '<div class="alert alert-warning" role="alert">'.__('bitte_einloggen').'</div>';
    	}
    	?>
    	<div class="alert alert-danger hidden" role="alert"><?php echo __('login_abgelaufen');?></div>
        <label for="passwort"><?php echo __('passwort1');?></label>
        <p class="redText"><?php echo __('passwort2');?></p>
        <form method="post" action="/index.php#archiv" class="row justify-content-center mb-1" id="loginForm">
        	<div class="form-group mr-sm-1 mb-2 col">
        		<input id="passwort" type="password" value="" name="passwort" class="form-control" placeholder="<?php echo __('password'); ?>">
        	</div>
        	<button id="login_button" class="btn mb-2 col" type="submit">Login</button>
        </form>
        <div class="small"><?php echo __('pw_small1');?><br /><?php echo __('pw_small2');?> <a href="mailto:fassade@szff.ch?Subject=<?php echo __('pw_subject');?>">fassade@szff.ch</a></div>
    </div>
</div>