<div class="col-md-12" id="impressum">
	<?php if(isset($_SESSION['language'] ) && $_SESSION['language']  == 1) { ?>
	<h1>Ours/contact</h1>

    <h3>Editeur</h3>
    <p>SZFF/CSFF<br />
    Schweizerische Zentrale Fenster und Fassaden SZFF<br>
    Ringstrasse 15<br />
    CH-4600 Olten<br />
    Telefon +41 62 287 40 00, Fax +41 62 287 40 0<br />
    <a href="mailto:fassade@szff.ch">fassade@szff.ch</a><br />
    <a href="http://www.szff.ch/" target="_blank">www.szff.ch</a></p>

    <h3>Rédaction</h3>
    <p>Fabio Rea (Rédacteur en chef), <a href="mailto:frea@szff.ch">frea@szff.ch</a><br>
    Susanne Bürli (Assistante du Directeur), <a href="mailto:sbuerli@szff.ch">sbuerli@szff.ch</a><br>
    Téléphone +41 62 287 40 00, Fax +41 62 287 40 09<br />
    <a href="">fassade@szff.ch</a></p>

    <h3>Collaboration rédactionnelle</h3>
    <p>Luigi Gino Angelini, CH-1000 Lausanne, <a href="mailto:angelini@preface.ch">angelini@preface.ch</a><br>
    Adrio D'Agostinis, CH-8057 Zürich, <a href="mailto:a.dagostinis@mebatech.ch">a.dagostinis@mebatech.ch</a><br>
    Rudolf Locher, CH-4600 Olten, <a href="mailto:rlocher@szff.ch">rlocher@szff.ch</a><br>
    Ubald Häring, CH-4600 Olten, <a href="mailto:uhaering@szff.ch">uhaering@szff.ch</a></p>

    <h3>Annonces</h3>
    SZFF/CSFF<br />
    Centrale Suisse Fenêtres et Façades CSFF<br />
    Ringstrasse 15<br />
    CH-4600 Olten<br />
    Téléphone +41 62 287 40 00, Fax +41 62 287 40 09<br />
    <a href="mailto:fassade@szff.ch">fassade@szff.ch</a><br />
    <a href="http://www.szff.ch/" target="_blank">www.szff.ch</a></p>

    <p>Impression<br />
    FO-Fotorotar AG<br />
    CH-8132 Egg<br />
    <a target="_blank" href="http://www.fo-fotorotar.ch">www.fo-fotorotar.ch</a></p>
    <?php }
          else { ?>
    <h1>Impressum/Kontakt</h1>

    <h3>Herausgeber und Verlag</h3>
    <p>SZFF/CSFF<br />
    Schweizerische Zentrale Fenster und Fassaden SZFF<br>
    Ringstrasse 15<br />
    CH-4600 Olten<br />
    Telefon +41 62 287 40 00, Fax +41 62 287 40 0<br />
    <a href="mailto:fassade@szff.ch">fassade@szff.ch</a><br />
    <a href="http://www.szff.ch/" target="_blank">www.szff.ch</a></p>

    <h3>Redaktion</h3>
    <p>Fabio Rea (verantwortlicher Redaktor), <a href="mailto:frea@szff.ch">frea@szff.ch</a><br>
    Susanne Bürli (Assistenz des Geschäftsleiters), <a href="mailto:sbuerli@szff.ch">sbuerli@szff.ch</a><br>
    Telefon +41 62 287 40 00, Fax +41 62 287 40 09<br />
    <a href="">fassade@szff.ch</a></p>

    <h3>Redaktionelle Mitarbeit</h3>
    <p>Luigi Gino Angelini, CH-1000 Lausanne, <a href="mailto:angelini@preface.ch">angelini@preface.ch</a><br>
    Adrio D'Agostinis, CH-8057 Zürich, <a href="mailto:a.dagostinis@mebatech.ch">a.dagostinis@mebatech.ch</a><br>
    Rudolf Locher, CH-4600 Olten, <a href="mailto:rlocher@szff.ch">rlocher@szff.ch</a><br>
    Ubald Häring, CH-4600 Olten, <a href="mailto:uhaering@szff.ch">uhaering@szff.ch</a></p>

    <h3>Anzeigen</h3>
    <p>SZFF/CSFF<br />
    Schweizerische Zentrale Fenster und Fassaden<br>
    Ringstrasse 15<br />
    CH-4600 Olten<br />
    Telefon +41 62 287 40 00, Fax +41 62 287 40 09<br />
    <a href="mailto:fassade@szff.ch">fassade@szff.ch</a><br />
    <a href="http://www.szff.ch/" target="_blank">www.szff.ch</a></p>

    <h3>Druck:</h3>
    <p>FO-Fotorotar AG<br />
    CH-8132 Egg<br />
    <a target="_blank" href="http://www.fo-fotorotar.ch">www.fo-fotorotar.ch</a></p>
    <?php } ?>
    <a href="/" class="btn backBtn"><?php echo __('zurueck');?></a>
</div>
