<?php
	//Lässt die Fehlermeldungen zum vorschein bringen
	ini_set('display_errors', true);
	error_reporting(E_ALL); 
	
	//Die Datei config.php, und die Klasse class.db.php werden eingebunden
	require_once("../config.php");
	require_once("../include/class.db.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SZFF Benutzer Hinzufügen</title>
<link type="text/css" rel="stylesheet" href="../stylesheet/style.css" />
</head>

<form action="" method="post">
	<fieldset id="benutzer_neu">
		<legend>Benutzer</legend>
		<div id="benutzer">
			<table>
				<tr>
					<td>Passwort: </td><td><input type="text" name="benutzer_passwort" id="benutzer_passwort"></td>
				</tr>
				<tr>
					<td>Gültigkeit des Passworts</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>Von </td>
					<td>
						<select name="passwort_start_tag">
							<?php
								for($t=1; $t<=31; $t++){
									echo '<option value="' . $t . '">' . $t . '</option>';
								}
							?>
						</select>
						<select name="passwort_start_monat">
							<?php
								for($m=1; $m<=12; $m++){
									echo '<option value="' . $m . '">' . $m . '</option>';
								}
							?>
						</select>
						<select name="passwort_start_jahr">
							<?php
								for($y=date("Y"); $y<=date("Y") + 2; $y++){
									echo '<option value="' . $y . '">' . $y . '</option>';
								}
							?>
						</select>(dd.mm.yyyy)
					</td>
				</tr>
				<tr>
					<td> Bis </td>
					<td> 
						<select name="passwort_end_tag">
							<?php
								for($t=1; $t<=31; $t++){
									echo '<option value="' . $t . '">' . $t . '</option>';
								}
							?>
						</select>
						<select name="passwort_end_monat">
							<?php
								for($m=1; $m<=12; $m++){
									echo '<option value="' . $m . '">' . $m . '</option>';
								}
							?>
						</select>
						<select name="passwort_end_jahr">
							<?php
								for($y=date("Y"); $y<=date("Y") + 2; $y++){
									echo '<option value="' . $y . '">' . $y . '</option>';
								}
							?>
						</select>(dd.mm.yyyy)
					</td>
				</tr>
				<tr>
					<td>Admin: </td><td>Ja <input type="checkbox" name="admin_ja" id="admin_ja" value="1"></td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td><input type="submit" name="benutzer_hinzufuegen" id="benutzer_hinzufuegen" value="Hinzufügen"></td>
				</tr>
			</table>
		</div>
		<p><h3>Hinweis:</h3>Adminpasswörter sind immer Gültig,<br/> normale Passwörter haben hingegen ein Start- und ein End-Datum!</p>
	</fieldset>
</form>

<?php
	$adminJa = 0;
	if(isset($_POST['benutzer_hinzufuegen'])){
		
		//Überprüft ob die Post's gesetzt sind, wenn gesetzt werden sie in eine Variable geschrieben
		if(isset($_POST['benutzer_passwort'])){
			$passwort = md5($_POST['benutzer_passwort']);
		}
		
		if(isset($_POST['passwort_start_tag'])){
			$anfangsTag = $_POST['passwort_start_tag'];
			$anfangsMonat = $_POST['passwort_start_monat'];
			$anfangsJahr = $_POST['passwort_start_jahr'];
		}
		
		if(isset($_POST['passwort_end_tag'])){
			$endTag = $_POST['passwort_end_tag'];
			$endMonat = $_POST['passwort_end_monat'];
			$endJahr = $_POST['passwort_end_jahr'];
		}
		
		//Wenn admin_ja gesetzt ist soll das Datum auf 0 gesetzt werden, da es nicht von bedeutung ist (weil Adminpasswort)
		if(isset($_POST['admin_ja'])){
			$adminJa = $_POST['admin_ja'];
			$anfangsTag = 0;
			$anfangsMonat = 0;
			$anfangsJahr = 0;
			$endTag = 0;
			$endMonat = 0;
			$endJahr = 0;
		}
	
	
		//Überprüft ob der Tag und der Monat kleiner als 10 sind, wenn ja dan soll eine 0 vor die Zahl gehängt werden
		if($anfangsTag < 10){
			$anfangsTag = "0" . $anfangsTag;
		}
		
		if($anfangsMonat < 10){
			$anfangsMonat = "0" . $anfangsMonat;
		}
		if($endTag < 10){
			$endTag = "0" . $endTag;
		}
		
		if($endMonat < 10){
			$endMonat = "0" . $endMonat;
		}

		
		//Überprüft ob die Variable anfangsTag = 0 wen ja dan ist der Timestamp auch 0 weil es ein Adminpasswort ist
		if($anfangsTag == 0){
			$anfangsTimestamp = 0;
		}else{
			//Verwandelt das Datum in einen Timestamp
			$anfangsTimestamp = mktime(0, 0, 0, $anfangsMonat, $anfangsTag, $anfangsJahr);
		}
		
		//Überprüft ob die Variable entTag = 0 wen ja dan ist der Timestamp auch 0 weil es ein Adminpasswort ist
		if($endTag == 0){
			$endTimestamp = 0;
		}else{
			//Verwandelt das Datum in einen Timestamp
			$endTimestamp = mktime(23, 59, 59, $endMonat, $endTag, $endJahr);
		}
		
		if(isset($_POST['benutzer_passwort']) && $_POST['benutzer_passwort']!=""){
			//Array erstellen, damit die Daten dan Per class.db.php in die Datenbank eingetragen wird.
			$felder = array('fld_passwort' => $passwort, 'fld_isAdmin' => $adminJa, 'fld_start' => $anfangsTimestamp, 'fld_end' => $endTimestamp);
			$result = $sql->fDoInsert("tbl_passwort", $felder);
			echo '<p id="benutzer_meldungen">Passwort wurde hinzugefügt!</p>';
		}else{
			echo '<p id="benutzer_meldungen">Bitte ein Passwort einfügen!</p>';
		}
	}
?>